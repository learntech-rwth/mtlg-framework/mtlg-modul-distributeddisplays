/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2019-05-14T10:47:48+02:00
 */
var locText = MTLG.lang.getString;
var demo = {};
 /**
  * Function that checks if this level should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
 var check = function(gameState) {
   if (gameState && gameState.nextLevel && gameState.nextLevel == "dotGame.level2") {
     return 1;
   }
   return 0;
 }

/**
 * Switches to the next field: Game
 */
var startGame = function () {
    if (demo.controlling) {
        demo.refreshFunction = null;
        demo.refreshInterval = Infinity;
        demo.field2 = {};
        demo.sharedRoom.inGame = true;
        MTLG.lc.levelFinished({
          nextLevel: "dotGame.level3",
          demo: demo
        });
    }
};


/**
 * Initialize the second field: Control selection
 */
var feld2_init = function (gamestate) {
    let stage = MTLG.getStageContainer();
    stage.removeAllChildren();
    demo = gamestate.demo;
    demo.members = {}; // Cache for the reversed demo.sharedRoom
    demo.refreshFunction = refreshMemberList;
    demo.refreshInterval = 300;//ms
    demo.nextRefresh = demo.refreshInterval;

    drawField2();
};

/**
 * Draw the second field: Control selection
 */
var drawField2 = function () {
    let stage = MTLG.getStageContainer();

    var w = demo.w;
    var h = demo.h;


    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, 100 * w, 100 * h);

    // Text
    var title = new createjs.Text(locText("field2_title"), demo.titleFont, demo.textColor);
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 50 * w;
    title.y = 10 * h;

    var id = MTLG.distributedDisplays.rooms.getClientId();
    var subTitle = new createjs.Text(locText("field2_subTitle1") + ", " + id + ", " + locText("field2_subTitle2") + " " + demo.roomName, demo.normalFont, demo.textColor);
    subTitle.textBaseline = 'alphabetic';
    subTitle.textAlign = 'center';
    subTitle.x = 50 * w;
    subTitle.y = 14 * h;

    // Members

    // Headline for the list of current members
    var currentMembersText = new createjs.Text(locText("field2_currentMembers")+ ": ", demo.normalFont, demo.textColor);
    currentMembersText.textBaseline = 'alphabetic';
    currentMembersText.x = 5 * w;
    currentMembersText.y = 21 * h;

    // Member list
    demo.field2.memberList = new createjs.Container();
    demo.field2.memberList.x = 5 * w;
    demo.field2.memberList.y = 25 * h;

    // Controls

    // Container
    var controlSelection = new createjs.Container();
    controlSelection.x = 10 * w;
    controlSelection.y = 60 * h;

    // Images for the buttons, the width of all is 12 * w
    var xImage = new createjs.Shape();
    xImage.graphics.beginFill(demo.textColor).drawRect(-4 * w, -0.5 * w, 8 * w, 1 * w).drawPolyStar(-4 * w, 0, 2 * w, 3, 0, 180).drawPolyStar(4 * w, 0, 2 * w, 3, 0, 0);
    var yImage = new createjs.Shape();
    yImage.graphics.beginFill(demo.textColor).drawRect(-0.75 * h, -6 * h, 1.5 * h, 12 * h).drawPolyStar(0, -6 * h, 3 * h, 3, 0, -90).drawPolyStar(0, 6 * h, 3 * h, 3, 0, 90);
    var piFrac = Math.PI / 6; // One twelth of a full circle
    var rotationImage = new createjs.Shape();
    rotationImage.graphics.setStrokeStyle(1 * w).beginStroke(demo.textColor).arc(0, 0, 4 * w, -5.5 * piFrac, 2 * piFrac);
    rotationImage.graphics.beginFill(demo.textColor).drawPolyStar(Math.cos(   2 * piFrac) * 4 * w, Math.sin(   2 * piFrac) * 4 * w, 0.5 * w, 3, 0, 143)
                                                    .drawPolyStar(Math.cos(-5.5 * piFrac) * 4 * w, Math.sin(-5.5 * piFrac) * 4 * w, 0.5 * w, 3, 0, 117);
    var sizeImage = new createjs.Shape();
    sizeImage.graphics.beginFill(demo.textColor).drawRect(-4 * w, -0.5 * w, 3 * w, 1 * w).drawRect(1 * w, -0.5 * w, 3 * w, 1 * w).drawPolyStar(-4 * w, 0, 1.5 * w, 3, 0, 180)
                                                                                                                                  .drawPolyStar( 4 * w, 0, 1.5 * w, 3, 0, 0);
    sizeImage.rotation = -45;

    // Buttons
    demo.field2.controlButtons = {};
    demo.field2.controlButtons.x = createImageButton(19 * w, 30 * h, xImage, locText("field2_memberControls_x"));
    demo.field2.controlButtons.y = createImageButton(19 * w, 30 * h, yImage, locText("field2_memberControls_y"));
    demo.field2.controlButtons.rotation = createImageButton(19 * w, 30 * h, rotationImage, locText("field2_memberControls_rotation"));
    demo.field2.controlButtons.size = createImageButton(19 * w, 30 * h, sizeImage, locText("field2_memberControls_size"));
    demo.field2.controlButtons.x.x = 0.5 * w;
    demo.field2.controlButtons.y.x = 20.5 * w;
    demo.field2.controlButtons.rotation.x = 40.5 * w;
    demo.field2.controlButtons.size.x = 60.5 * w;
    demo.field2.controlButtons.x.on('click', controlClickedHandler, null, false, "x");
    demo.field2.controlButtons.y.on('click', controlClickedHandler, null, false, "y");
    demo.field2.controlButtons.rotation.on('click', controlClickedHandler, null, false, "rotation");
    demo.field2.controlButtons.size.on('click', controlClickedHandler, null, false, "size");

    controlSelection.addChild(demo.field2.controlButtons.x);
    controlSelection.addChild(demo.field2.controlButtons.y);
    controlSelection.addChild(demo.field2.controlButtons.rotation);
    controlSelection.addChild(demo.field2.controlButtons.size);

    // Other buttons

    // Start the game
    var text = (demo.sharedRoom && demo.sharedRoom.inGame) ? "field2_joinGameButton" : "field2_startButton";
    demo.field2.startButton = MTLG.utils.uiElements.addButton({
      text: locText(text),
      sizeX: 10 * w,
      sizeY: 6 * h,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: 6 * h / 10
    }, startGame);
    demo.field2.startButton.x = 90 * w - demo.field2.startButton.getBounds().width / 2;
    demo.field2.startButton.y = 50 * h;

    // Join game note
    demo.field2.joinGameNote = new createjs.Text(locText("field2_joinGameNote"), demo.normalFont, demo.textColor);
    demo.field2.joinGameNote.textBaseline = 'alphabetic';
    demo.field2.joinGameNote.textAlign = 'right';
    demo.field2.joinGameNote.x = 90 * w;
    demo.field2.joinGameNote.y = 44 * h;
    demo.field2.joinGameNote.visible = demo.sharedRoom && demo.sharedRoom.inGame;

    // Leave the room
    var leaveRoomButton = MTLG.utils.uiElements.addButton({
      text: locText("field2_leaveRoomButton"),
      sizeX: 10 * w,
      sizeY: 3 * h,
      font: demo.font,
      textColor: demo.textColor,
      bgColor1: demo.buttonBackgroundColor,
      bgColor2: "black",
      radius: 3 * h / 10
    }, leaveRoom);
    leaveRoomButton.x = 90 * w - leaveRoomButton.getBounds().width / 2;
    leaveRoomButton.y = 19 * h;

    stage.addChild(background);
    stage.addChild(title);
    stage.addChild(subTitle);
    stage.addChild(currentMembersText);
    stage.addChild(demo.field2.memberList);
    stage.addChild(controlSelection);
    stage.addChild(demo.field2.startButton);
    stage.addChild(demo.field2.joinGameNote);
    stage.addChild(leaveRoomButton);
    //drawFullscreenButton();
};


/**
 * Refreshes the members of the joined room
 */
var refreshMemberList = function () {
    MTLG.distributedDisplays.rooms.getJoinedRooms(function (result) {
        if (result && !(result.success === false)) {
            if (result[demo.roomName]) {
                demo.members = result[demo.roomName].sockets;
                demo.controlling = false;
                var id = MTLG.distributedDisplays.rooms.getClientId();

                if (demo.sharedRoom) {
                    var controls = demo.sharedRoom.controls;
                    for (var i in controls) {
                        if (demo.field2.controlButtons) demo.field2.controlButtons[i].changeState(demo.textColor, ""); // If is required for a late response after switching to game

                        // For each control, add the info to the member who controls it
                        if (controls[i]) { // If the control is assigned to a player
                            if (demo.members[controls[i]]) {
                                demo.members[controls[i]] = i; // If that player is a member, save the control info for that member
                                var color = demo.disabledColor;
                                if (controls[i] === id) {
                                    color = demo.selectedColor;
                                    demo.controlling = i;
                                }
                                if (demo.field2.controlButtons) demo.field2.controlButtons[i].changeState(color, controls[i]); // Show that a player is assigned to this control
                            } else {
                                demo.sharedRoom.controls[i] = false; // Remove the assigned player from that control, since he is not a member of this room
                            }
                        }
                    }
                } else { // No sharedRoom object
                    if (result[demo.roomName].length <= 1 && demo.members[id]) {
                        // This client is the only client in the room, so a new sharedRoom is created
                        demo.sharedRoom = createSharedRoomObject(demo.roomName);
                    } else {
                        // Request the sharedRoom object from the other members
                        MTLG.distributedDisplays.communication.sendCustomAction(demo.roomName, "getSharedRoom", id, demo.roomName);
                    }
                }

                if (demo.field2.memberList) {
                    // Display the member list
                    demo.field2.memberList.removeAllChildren();
                    var y = 0;
                    for (let memberName in demo.members) {
                        var text = new createjs.Text(memberName + ': ' + locText("field2_memberControls_" + demo.members[memberName] ), demo.normalFont, demo.textColor);
                        text.y = y;
                        demo.field2.memberList.addChild(text);
                        y += 4 * demo.h;
                    }

                    // Enable / disable the start button
                    if (demo.controlling) {
                        demo.field2.startButton.children[0].graphics._fill.style = demo.buttonBackgroundColor;
                        demo.field2.startButton.children[1].color = demo.textColor;
                    } else {
                        demo.field2.startButton.children[0].graphics._fill.style = demo.disabledBackgroundColor;
                        demo.field2.startButton.children[1].color = demo.disabledColor;
                    }
                }
            }
        }
    });
};

/**
 * Creates a new button like element, bigger button with image and text
 * @param {number} w The width of the button
 * @param {number} h The height of the button
 * @param {DisplayObject} image The Shape, Bitmap or other DisplayObject to use as central image
 * @param {string} text The text to display on the button#
 * @returns {Container} The created button element
 */
var createImageButton = function (w, h, image, text) {
    var buttonContainer = new createjs.Container();

    var textObj = new createjs.Text(text, demo.buttonFont, demo.textColor);
    textObj.textBaseline = 'alphabetical';
    textObj.textAlign = 'center';
    textObj.x = w / 2;
    textObj.y = h - demo.buttonMargin;

    var subText = new createjs.Text("", demo.buttonFont, demo.disabledColor);
    subText.textBaseline = 'bottom';
    subText.textAlign = 'center';
    subText.x = w / 2;
    subText.y = h;

    var back = new createjs.Shape();
    back.graphics.beginStroke(demo.disabledColor).beginFill(demo.transparentColor).drawRect(0, 0, w, h); // Background FFF fill is for the click handler

    image.x = w / 2;
    image.y = h / 3;

    buttonContainer.addChild(back);
    buttonContainer.addChild(textObj);
    buttonContainer.addChild(subText);
    buttonContainer.addChild(image);

    buttonContainer.changeState = function (color, subtext) {
        textObj.color = color;
        if (image.graphics._fill) image.graphics._fill.style = color;
        if (image.graphics._stroke) image.graphics._stroke.style = color;
        subText.text = subtext;
    };

    return buttonContainer;
};

/**
 * Creates a SharedObject for room information, it is no CreateJS object
 * @param {string} roomName The name of the room
 * @returns {object} The created shared room object, which is wrapped by a SharedObject
 */
var createSharedRoomObject = function (roomName) {
    var character = new createjs.Container();
    var dot1 = new createjs.Shape();
    dot1.graphics.beginFill(demo.characterColor).drawCircle(0, 0, 1);
    dot1.x = -7;
    var dot2 = new createjs.Shape();
    dot2.graphics.beginFill(demo.characterColor).drawCircle(0, 0, 1);
    dot2.x = 7;
    character.addChild(dot1);
    character.addChild(dot2);
    // Trigger an update of the _instructions Array
    dot1.graphics.instructions;
    dot2.graphics.instructions;

    var roomObj = {
        controls: { // The assigned controls
            x: false, // False indicates that the control is free, a client id indicates who controls the element
            y: false,
            rotation: false,
            size: false
        },
        character: character, // The synchronized character itself
        debug: { // For remote control
            velocityX: 0,
            velocityY: 0,
            rotation: 0,
            size: 7
        },
        gameOver: false, // Lost
        inGame: false, // Is the group currently playing a level
        name: roomName // The name of the room
    };
    // Wrap the roomObj with a SharedObject, but postpone the initialization
    var sharedObj = new MTLG.distributedDisplays.SharedObject(roomObj, false, true);
    // Configure the properties to synchronize
    sharedObj.syncProperties = {
        controls: {
            send: true, receive: true, className: "Object",
            subProperties: {
                x:        { send: true, receive: true },
                y:        { send: true, receive: true },
                rotation: { send: true, receive: true },
                size:     { send: true, receive: true }
            }
        },
        character: { send: true, receive: true, className: "SharedObject" },
        debug: {
            send: true, receive: true, className: "Object",
            subProperties: {
                velocityX: { send: true, receive: true },
                velocityY: { send: true, receive: true },
                rotation:  { send: true, receive: true },
                size:      { send: true, receive: true }
            }
        },
        gameOver: { send: true, receive: true },
        inGame:   { send: true, receive: true },
        name:     { send: true, receive: true }
    };
    // Initialize the sharedObj to detect changes
    sharedObj.init();
    return roomObj;
};

/**
 * Handler for the press of the control selection buttons
 * @param {object} event The click event
 * @param {string} control The name of the clicked control
 */
var controlClickedHandler = function (event, control) {
    var id = MTLG.distributedDisplays.rooms.getClientId();
    var controls = demo.sharedRoom.controls;
    if (controls[control]) {
        // Control is assigned, if to self then unassign, otherwise noop
        if (controls[control] === id) {
            controls[control] = false; // Unassign control
            demo.field2.controlButtons[control].changeState(demo.textColor, "");
        }
    } else {
        // Control is not assigned, assign to self
        var existing = false;
        for (var c in controls) {
            if (c === control) existing = true;
            if (controls[c] === id) {
                controls[c] = false; // Unassing other selected control
                demo.field2.controlButtons[c].changeState(demo.textColor, "");
            }
        }
        // Assing new control if existing
        if (existing) {
            controls[control] = id;
            demo.field2.controlButtons[control].changeState(demo.selectedColor, id);
        }
    }
};

/**
* Leaves the room and switches to the previous field: Room selection
*/
var leaveRoom = function () {
   MTLG.distributedDisplays.rooms.leaveRoom(demo.roomName, function (result) {
       if (result && result.success) {
           demo.refreshFunction = null;
           demo.refreshInterval = Infinity;
           demo.roomName = "";
           MTLG.distributedDisplays.SharedObject.getSharedObject(demo.sharedRoom.sharedId).delete();
           demo.sharedRoom = null;
           demo.field2 = {};
           MTLG.lc.levelFinished({
             nextLevel: "dotGame",
             demo: demo
           });
       } else {
           swal.fire({
               type: 'error',
               title: locText("field1_leaveRoomErrorTitle"),
               text: result.reason,
               focusConfirm: true
           });
       }
   });
};

module.exports = {
  init: feld2_init,
  check: check
}
