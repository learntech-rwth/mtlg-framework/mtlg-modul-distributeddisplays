/**
 * @Date:   2019-03-19T10:14:00+01:00
 * @Last modified time: 2019-05-15T09:28:22+02:00
 */


var locText = MTLG.lang.getString;
/**
 * Function that checks if level1 should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var checkLevel1 = function(gameState){
  if (gameState && gameState.nextLevel && gameState.nextLevel == "linkedAreaDemo") {
    return 1;
  }
  return 0;
}

/**
 * Initialize the first field: Room selection
 */
var level1_init = function () {
    let stage = MTLG.getStageContainer();
    // try to create room. If the room will be created, this was the first
    // attempt to create it and therefore this client is the master. If it is
    // not possible to create the room, then the room is already created and
    // there is already a master, therefore this client is a slave.
    MTLG.distributedDisplays.rooms.createRoom('room', function(result){
      if(result && result.success){
        // This client is a master and therefore passes true to the draw function
        drawField1(true);
      }else{
        // This client is a slave. Now it tries to join the room
        MTLG.distributedDisplays.rooms.joinRoom('room', function(result) {
          if(result && result.success){
            // if joined, the field should be drawn with false (for not master)
            // as parameter
            drawField1(false);
          }
        });
      }
    });
};

/**
 * Draw the first field: Room selection
 */
var drawField1 = function (master) {
    let stage = MTLG.getStageContainer();

    // for legible code
    let w = MTLG.getOptions().width;
    let h = MTLG.getOptions().height;

    // Background
    var background = new createjs.Shape();
    background.graphics.beginFill("#FFF").drawRect(0, 0, w, h);

    // Text
    // the text for the title depends on the role.
    var titleText;
    if(master) titleText = locText("master");
    else titleText = locText("slave");

    var title = new createjs.Text(titleText, "40px Arial", "Black");
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 0.5 * w;
    title.y = 0.1 * h;

    if(master){
      // If master the four blueCircles will be created (for the four linkedAreas)
      var blueCircle = MTLG.utils.gfx.getShape();
      blueCircle.graphics.beginFill('blue').drawCircle(0,0,50);
      blueCircle.x = 1/5 * w;
      blueCircle.y = 0.5 * h;
      // Add listener to be able to move the blueCircle
      blueCircle.on('pressmove', pressMoveFunction);
      // The blueCircle should be shared, therefore in the next line an
      // SharedObject from the blueCircle will be instantiated
      blueCircleObject = new MTLG.distributedDisplays.SharedObject(blueCircle,0);

      var text_la_f_f = new createjs.Text(locText("la_f_f"), "25px Arial", "Black");
      text_la_f_f.textBaseline = 'alphabetic';
      text_la_f_f.textAlign = 'center';
      text_la_f_f.x = 1/5 * w;
      text_la_f_f.y = 1/5 * h - 10;

      var blueCircle2 = MTLG.utils.gfx.getShape();
      blueCircle2.graphics.beginFill('blue').drawCircle(0,0,50);
      blueCircle2.x = 2/5 * w;
      blueCircle2.y = 0.5 * h;
      blueCircle2.on('pressmove', pressMoveFunction);
      let blueCircleObject2 = new MTLG.distributedDisplays.SharedObject(blueCircle2,0);

      var text_la_f_t = new createjs.Text(locText("la_f_t"), "25px Arial", "Black");
      text_la_f_t.textBaseline = 'alphabetic';
      text_la_f_t.textAlign = 'center';
      text_la_f_t.x = 2/5 * w;
      text_la_f_t.y = 1/5 * h - 10;

      var blueCircle3 = MTLG.utils.gfx.getShape();
      blueCircle3.graphics.beginFill('blue').drawCircle(0,0,50);
      blueCircle3.x = 3/5 * w;
      blueCircle3.y = 0.5 * h;
      blueCircle3.on('pressmove', pressMoveFunction);
      let blueCircleObject3 = new MTLG.distributedDisplays.SharedObject(blueCircle3,0);

      var text_la_t_f = new createjs.Text(locText("la_t_f"), "25px Arial", "Black");
      text_la_t_f.textBaseline = 'alphabetic';
      text_la_t_f.textAlign = 'center';
      text_la_t_f.x = 3/5 * w;
      text_la_t_f.y = 1/5 * h - 10;

      var blueCircle4 = MTLG.utils.gfx.getShape();
      blueCircle4.graphics.beginFill('blue').drawCircle(0,0,50);
      blueCircle4.x = 4/5 * w;
      blueCircle4.y = 0.5 * h;
      blueCircle4.on('pressmove', pressMoveFunction);
      let blueCircleObject4 = new MTLG.distributedDisplays.SharedObject(blueCircle4,0);

      var text_la_t_t = new createjs.Text(locText("la_t_t"), "25px Arial", "Black");
      text_la_t_t.textBaseline = 'alphabetic';
      text_la_t_t.textAlign = 'center';
      text_la_t_t.x = 4/5 * w;
      text_la_t_t.y = 1/5 * h - 10;

    }else{
      // If you want that something happens when you receive a new object,
      // you have to add a listener for the addSharedObjects event like this.
      // Here we want that the incoming Circles are moveable therefore we
      // have to add the pressmoveListener
      MTLG.distributedDisplays.actions.registerListenerForAction('addSharedObjects', addSharedObjectsListener);
    }

    // Button to get back to the main menu
    var backButton = MTLG.utils.uiElements.addButton({
      text: locText("back"),
      sizeX: w * 0.1,
      sizeY: 70,
      textColor: 'black',
      bgColor1: 'white',
      bgColor2: 'red'
    }, function(){
      // Leave the room and delete the sharedObject, so that no updates will be
      // received in the future.
      MTLG.distributedDisplays.rooms.leaveRoom('room', function (result) {
          if (result && result.success) {
              MTLG.utils.inactivity.removeAllListeners();
              MTLG.distributedDisplays.actions.deregisterListenerForAction('addSharedObjects', addSharedObjectsListener);
              MTLG.lc.goToMenu();
          } else {
              swal({
                  type: 'error',
                  title: locText("field1_leaveRoomErrorTitle"),
                  text: result.reason,
                  focusConfirm: true
              });
          }
      });

    });
    backButton.x =  w - w * 0.1;
    backButton.y = h - 70;

    // Add objects to stage
    stage.addChild(background);
    stage.addChild(title);
    if(master) {
      stage.addChild(blueCircle);
      stage.addChild(blueCircle2);
      stage.addChild(blueCircle3);
      stage.addChild(blueCircle4);

      // Add LinkedAreas
      // Here are four different areas added. The first one sends the object
      // and then it is synchronized.
      stage.addChild(text_la_f_f);
      new MTLG.distributedDisplays.LinkedArea( w * 1/5 - w * 0.05, h * 0.25 - h * 0.05, w * 0.1, h * 0.1, true, "room", false, false);
      // The second one sends the object, but delete it after the sending,
      // then the object only exists for the receiver.
      stage.addChild(text_la_f_t);
      new MTLG.distributedDisplays.LinkedArea( w * 2/5 - w * 0.05, h * 0.25 - h * 0.05, w * 0.1, h * 0.1, true, "room", false, true);
      // The third one sends a copy
      stage.addChild(text_la_t_f);
      new MTLG.distributedDisplays.LinkedArea( w * 3/5 - w * 0.05, h * 0.25 - h * 0.05, w * 0.1, h * 0.1, true, "room", true, false);
      // Here a copy will be send and the object itself will be deleted here.
      stage.addChild(text_la_t_t);
      new MTLG.distributedDisplays.LinkedArea( w * 4/5 - w * 0.05, h * 0.25 - h * 0.05, w * 0.1, h * 0.1, true, "room", true, true);

    }else {
      // Here it will be asked for all joined rooms in order to get the first
      // clientID of the room called 'room'. Because the first in the room is
      // in this scenario (!) the master
      MTLG.distributedDisplays.rooms.getJoinedRooms(function (rooms) {
        if(rooms.room){
          // first in the room is the master and therefore the receiver for the linkedArea
          var receiver = Object.keys(rooms.room.sockets)[0];
          //add LinkedArea in order to get the sent objects here.
          new MTLG.distributedDisplays.LinkedArea(w * 0.5 - w * 0.125, h * 0.5 - h * 0.125, w * 0.25, h * 0.25, true, receiver, false, false);
        } else {
          // TODO: Error handling
        }
      });
    }

    //Back button to go to menu
    stage.addChild(backButton);
};

var addSharedObjectsListener = function({action, added, from, toAdd}){
  //Add here what should happen
  var sharedId = Object.keys(added)[0];
  var object = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
  object.createJsObject.on('pressmove', pressMoveFunction);
}

/**
 * This function is a listener for the blue circle.
 * It updates the location of the blueCircle.
 */
var pressMoveFunction = function({localX, localY}){
    this.set(this.localToLocal(localX, localY, this.parent));
};


module.exports = {
  checkLevel1: checkLevel1,
  level1_init: level1_init
}
