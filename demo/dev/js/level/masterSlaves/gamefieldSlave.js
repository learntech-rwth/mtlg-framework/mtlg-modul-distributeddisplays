/**
 * @Date:   2019-03-26T13:20:42+01:00
 * @Last modified time: 2019-05-15T16:33:22+02:00
 */
 try {
    var utils = require('./utils.js');
 } catch (error) {
   utils = utils || {};
 }

var locText = MTLG.lang.getString;
// gobal variables
var infos = {};
var linkedAreaMaster;
var linkedAreaPlayer;
 /**
  * Function that checks if this level should be the next level
  * @param the current game state (an object containing only nextLevel in this case)
  * @return a number in [0,1] that represents that possibility.
  */
 var checkGamefield = function(gameState){
   if (gameState && gameState.nextLevel && gameState.nextLevel == "basicMasters.gamefieldSlave") {
     return 1;
   }
   return 0;
 }

 /**
  *  This function initialises the info variable and calls the draw function
  *
  * @param  {type} gameState Current state of the game (should include the 'infos' variable)
  */
 var gamefield_init = function(gameState){
   infos = {};
   linkedAreaMaster = null;
   linkedAreaPlayer = null;

   if(gameState.infos) {
     infos = gameState.infos;

     MTLG.distributedDisplays.actions.registerListenerForAction('updateSharedObject', updateSharedInfos);
   } else {
     // Request Infos

   }
   //register this Listner to react on adding shared Objects.
   MTLG.distributedDisplays.actions.registerListenerForAction('addSharedObjects', addSharedObjectsListener);

   draw();


 }


 /**
  * This function draws the player game field
  *
  */
 var draw = function () {
   var stage = MTLG.getStageContainer();
   var gWidth = MTLG.getOptions().width;
   var gHeight = MTLG.getOptions().height;

   // White Background Rectangle
   var background = new createjs.Shape();
   background.graphics.beginFill('white').drawRect(0, 0, gWidth, gHeight);
   coordinates = {
     xCoord: gWidth / 2,
     yCoord: gHeight / 2
   };
   background.regX = gWidth / 2;
   background.regY = gHeight / 2;
   background.x = coordinates.xCoord;
   background.y = coordinates.yCoord;
   stage.addChild(background);

   // linkedArea for master, if exits
   if(infos.sharedInfos.master != null) linkedAreaMaster = utils.createLinkedArea(gWidth/8, gHeight/5, gWidth/4, gHeight/4, infos.sharedInfos.master, locText('master'));

   // linkedArea for other player, if exists
   if(infos.sharedInfos.players.length >= 2) {
     var receiver = null;
     for(let player in infos.sharedInfos.players) {
       if(infos.sharedInfos.players[player] != MTLG.distributedDisplays.rooms.getClientId()) {
         receiver = infos.sharedInfos.players[player];
         break;
       }
     }
     if(receiver == null) {
       console.error('Receiver (other player) of LinkedArea not found!');
     } else {
       linkedAreaPlayer = utils.createLinkedArea(gWidth * 5/8, gHeight/5, gWidth/4, gHeight/4, receiver, locText('otherPlayer'));
     }
   }

   // Button to get back to the main menu
   var backButton = MTLG.utils.uiElements.addButton({
     text: locText("back"),
     sizeX: gWidth * 0.1,
     sizeY: 70,
     textColor: 'black',
     bgColor1: 'white',
     bgColor2: 'red'
   }, function(){

       var index = infos.sharedInfos.players.indexOf(MTLG.distributedDisplays.rooms.getClientId());
       infos.sharedInfos.players.splice(index, 1);
     // Leave the room and delete the sharedObject, so that no updates will be
     // received in the future.
     MTLG.distributedDisplays.rooms.leaveRoom(infos.room, function (result) {
         if (result && result.success) {
           MTLG.utils.inactivity.removeAllListeners();
           MTLG.distributedDisplays.actions.removeCustomFunction("requestSharedInfos");
           MTLG.distributedDisplays.actions.deregisterListenerForAction('addSharedObjects', addSharedObjectsListener);
           MTLG.distributedDisplays.actions.deregisterListenerForAction('updateSharedObject', updateSharedInfos);
           MTLG.distributedDisplays.SharedObject.getSharedObject(infos.sharedInfos.sharedId).delete();
           delete infos.sharedInfos;
           infos.initialized = false;
           MTLG.lc.goToMenu();
         } else {
             swal({
                 type: 'error',
                 title: locText("field1_leaveRoomErrorTitle"),
                 text: result.reason,
                 focusConfirm: true
             });
            infos.sharedInfos.players.splice(index, 1, MTLG.distributedDisplays.rooms.getClientId());
         }
     });

   });
   backButton.x =  gWidth - gWidth * 0.1;
   backButton.y = gHeight - 70;
   stage.addChild(backButton);
 }


 /**
  * This function is called, when new sharedObjects are added.
  * The objects then get the ability to be moved by the player.
  *
  * @param  {type} {action The action ('addSharedObjects')
  * @param  {type} added   The sharedObjects that were added
  * @param  {type} from    The sender of the objects
  */
 var addSharedObjectsListener = function({action, added, from}){

   var sharedId = Object.keys(added)[0];
   if(sharedId != infos.sharedInfos.sharedId) {
     var object = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
     object.createJsObject.on('pressmove', utils.cards.moveCard);
   }
 }


 /**
  * This function updates the stage. If a player left the game, the corresponding
  * LinkedArea will be deleted.
  * If a player joinded the game, a linkedArea should be created.
  *
  * @param  {type} update the update object
  */
 var updateSharedInfos = function(update) {
   var w = MTLG.getOptions().width;
   var h = MTLG.getOptions().height;
   if(update.sharedId == infos.sharedInfos.sharedId) {
     var color = 'white';
     // If the master parameter has been updated, the button must be updated
     if (update.propertyPath == 'master') {
       if (infos.sharedInfos.master == null) {
         // delete linkedArea
         utils.removeLinkedArea(linkedAreaMaster);
       } else {
         linkedAreaMaster = utils.createLinkedArea(w/8, h/5, w/4, h/4, infos.sharedInfos.master, locText('master'));
       }
     }

     if(update.propertyPath.startsWith('players')) {
       if ( infos.sharedInfos.players.length == 2) {
         var receiver = null;
         for(let player in infos.sharedInfos.players) {
           if(infos.sharedInfos.players[player] != MTLG.distributedDisplays.rooms.getClientId()) {
             receiver = infos.sharedInfos.players[player];
             break;
           }
         }
         // linked Area must be drawn
         if (linkedAreaPlayer) {
           // already exists. Delete it!
           utils.removeLinkedArea(linkedAreaPlayer);
         } else {
           linkedAreaPlayer = utils.createLinkedArea(w * 5/8, h/5, w/4, h/4, receiver, locText('otherPlayer'));
         }
       } else {
         // delete LinkedArea
         if( linkedAreaPlayer) {
           utils.removeLinkedArea(linkedAreaPlayer);
         }
       }
     }
   }
 }

 module.exports = {
   check: checkGamefield,
   init: gamefield_init
 }
