/**
 * @Date:   2019-04-02T11:34:33+02:00
 * @Last modified time: 2019-05-15T14:19:13+02:00
 */
var locText = MTLG.lang.getString;
var swal = require('sweetalert2');

// gobal variables
var infos = {};
var tableButton;
var playerButton;
/**
 * Function that checks if this level should be the next level
 * @param the current game state (an object containing only nextLevel in this case)
 * @return a number in [0,1] that represents that possibility.
 */
var check = function(gameState) {
  if (gameState && gameState.nextLevel && gameState.nextLevel == "basicMasters.roleallocation") {
    return 1;
  }
  return 0;
}


/**
 * This function initializes the roleallocation. It gets the shared infos from
 * another player or initializes them. After that the draw function is called.
 *
 * @param  {type} gameState description
 * @return {type}           description
 */
var init = function(gameState) {
  infos = gameState.infos;
  if (!infos.initialized) {
    // Get SharedInfos, if another player is already in the room, otherwise
    // create the sharedInfos
    initSharedInfos().then(function(){
      infos.initialized = true;
      // register an update listener so that the visualisation can be changed on changes
      MTLG.distributedDisplays.actions.registerListenerForAction('updateSharedObject', updateSharedInfos);
      draw();
    }, function(err){
      swal.fire({
        type: 'error',
        title: locText('Oops...'),
        text: err
      });
    });
  } else {
    draw();
  }
}


/**
 * This function draws the roleallocation
 *
 */
var draw = function() {
  var stage = MTLG.getStageContainer();
  var gWidth = MTLG.getOptions().width;
  var gHeight = MTLG.getOptions().height;

  // White Background Rectangle
  var background = new createjs.Shape();
  background.graphics.beginFill('white').drawRect(0, 0, gWidth, gHeight);
  coordinates = {
    xCoord: gWidth / 2,
    yCoord: gHeight / 2
  };
  background.regX = gWidth / 2;
  background.regY = gHeight / 2;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;

  // color for role buttons
  var color = 'white';
  if (infos.sharedInfos.master != null) color = '#d9d9d9';

  // Button for the main gamefield (table)
  tableButton = MTLG.utils.uiElements.addButton({
    text: locText("table"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: color,
    bgColor2: 'black'
  }, function() {
    if (infos.sharedInfos.master != null) {
      // In this game there are only two players and one table allowed.
      swal.fire({
        type: 'error',
        title: locText('error'),
        text: locText('only_one_master')
      });
    } else {
      // set in the shared infos, that this client is the master.
      infos.sharedInfos.master = MTLG.distributedDisplays.rooms.getClientId();
      //deregister the listener because in the next level it is no longer needed.
      MTLG.distributedDisplays.actions.deregisterListenerForAction('updateSharedObject', updateSharedInfos);

      MTLG.lc.levelFinished({
        nextLevel: 'basicMasters.gamefieldMaster',
        infos: infos
      });
    }
  });
  tableButton.x = gWidth * 0.25;
  tableButton.y = gHeight * 0.5;

  // Button for the players
  if (infos.sharedInfos.players.length >= 2) color = '#d9d9d9';
  else color = 'white';
  playerButton = MTLG.utils.uiElements.addButton({
    text: locText("playerButton"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: color,
    bgColor2: 'black'
  }, function() {
    if (infos.sharedInfos.players.length >= 2) {
      // In this game there are only two players and one table allowed.
      swal.fire({
        type: 'error',
        title: locText('error'),
        text: locText('only_two_players')
      });
    } else {
      // set in the shared infos, that this client is one of the players
      infos.sharedInfos.players.push(MTLG.distributedDisplays.rooms.getClientId());
      //deregister the listener because in the next level it is no longer needed.
      MTLG.distributedDisplays.actions.deregisterListenerForAction('updateSharedObject', updateSharedInfos);

      MTLG.lc.levelFinished({
        nextLevel: 'basicMasters.gamefieldSlave',
        infos: infos
      });
    }
  });
  playerButton.x = gWidth * 0.75;
  playerButton.y = gHeight * 0.5;

  // add all objects to the stageContainer
  stage.addChild(background);
  stage.addChild(tableButton);
  stage.addChild(playerButton);
};


/**
 * This function initializes the shared Infos. If another player already did
 * that, the shared infos are retrieved from them
 *
 * @return {Promise}  Gives a promise back
 */
var initSharedInfos = function() {
  return new Promise(function(resolve, reject) {
    MTLG.distributedDisplays.rooms.getJoinedRooms(function(rooms) {
      if (rooms[infos.room]) {
        let length = rooms[infos.room].length;
        // if the length of the room is higher than 1, there is already another client
        // so that we can request the sharedInfos
        if (length > 1) {
          // Set customAction to receive the sharedObject
          MTLG.distributedDisplays.actions.setCustomFunction('receiveSharedInfos', function(sharedInfos) {
              //add the shared object
              MTLG.distributedDisplays.actions.addSharedObjects(sharedInfos);
              // Get the sharedId from the associative list
              var sharedId;
              for (var i in sharedInfos) {
                if (sharedInfos[i].sharedId) {
                  sharedId = sharedInfos[i].sharedId;
                  break;
                }
              }
              // Get the object in order to get the "createJS-Object" to save it.
              // It is not really a createJSObject but a plain JS Object.
              var sharedInfosObject = MTLG.distributedDisplays.SharedObject.getSharedObject(sharedId);
              infos.sharedInfos = sharedInfosObject.createJsObject;
              // register the following listener in order to send other players the shared infos.
              MTLG.distributedDisplays.actions.setCustomFunction('requestSharedInfos', customFunctionRequestSharedInfos);
              MTLG.distributedDisplays.actions.removeCustomFunction("receiveSharedInfos");
              resolve();
          });

          // in case no one answers, set a timeout for 10s in order to throw an error
          setTimeout(function() {
            reject(locText("NoAnswer"));
          }, 10000)
          // Request sharedInfos
          MTLG.distributedDisplays.communication.sendCustomAction(infos.room, 'requestSharedInfos', MTLG.distributedDisplays.rooms.getClientId());

        } else {
          // This client is the first in the room and must create the sharedInfos
          infos.sharedInfos = {
            master: null,
            players: []
          }
          // Postpone init because the sharedInfos object is not a createJS-Object
          // and therefore we need to declare the syncProperties
          // in order to sync what we want
          var object = new MTLG.distributedDisplays.SharedObject(infos.sharedInfos, false, true);
          object.syncProperties = {};

          object.syncProperties = {
            master: {send: true, receive: true},
            players: {
              send: true, receive: true, className: "Array", // Array of players
              itemClasses: {
                  length: { send: true, receive: true },
                  other:  { send: true, receive: true }
              }
            }
          }

          // after declaring the syncProperties we can init the object.
          object.init();

          // Set the custom function to send new members of the group the sharedInfos.
          MTLG.distributedDisplays.actions.setCustomFunction('requestSharedInfos', customFunctionRequestSharedInfos);
          resolve();
        }
      } else {
        reject(locText("Room not available"));
      }
    });
  });
};


/**
 * This function sends a message to the room so that they send the shared infos.
 *
 */
var customFunctionRequestSharedInfos = function() {
  if (infos.sharedInfos.sharedId) {
    var sharedObject = MTLG.distributedDisplays.SharedObject.getSharedObject(infos.sharedInfos.sharedId);
    MTLG.distributedDisplays.communication.sendCustomAction(infos.room, 'receiveSharedInfos', sharedObject.toSendables());
  }
};


/**
 * This function is called, when a shared object changes. It changes then the
 * visualisation on the stage.
 *
 * @param  {type} update Object, with the update infos
 */
var updateSharedInfos = function(update) {
  if(update.sharedId == infos.sharedInfos.sharedId) {
    var color = 'white';
    // If the master parameter has been updated, the button must be updated
    if (update.propertyPath == 'master') {
      if (infos.sharedInfos.master != null) color = '#d9d9d9';
      // Change background of table button
      // Look for background
      for (var child in tableButton.children) {
        if (tableButton.children[child].name == 'background') {
          var bg = tableButton.children[child];
          bg.graphics._fill.style = color;
          break;
        }
      }
    }

    if(update.propertyPath.startsWith('player')) {
      if (infos.sharedInfos.players.length >= 2) color = '#d9d9d9';
      // Change background of table button
      // Look for background
      for (var child in playerButton.children) {
        if (playerButton.children[child].name == 'background') {
          var bg = playerButton.children[child];
          bg.graphics._fill.style = color;
          break;
        }
      }
    }
  }

}

module.exports = {
  check: check,
  init: init
}
