/**
 * @Date:   2019-03-26T13:03:19+01:00
 * @Last modified time: 2019-05-15T16:45:26+02:00
 */

var swal = require('sweetalert2');
var locText = MTLG.lang.getString;


/**
 * This function asks for a name and creates a room with the name.
 *
 * @param  {type} callback Callback, which is called after successfully creating the room
 * @return {type}          description
 */
var addRoom = function(callback) {
  swal.fire({
    type: 'question',
    title: locText("field1_addRoomTitle"),
    text: locText("field1_addRoomQuestion"),
    input: 'text',
    inputPlaceholder: locText("field1_addRoomPlaceholder"),
    allowEnterKey: true,
    focusConfirm: false,
    showCancelButton: true,
    inputValidator: function(value) {
      return new Promise(function(resolve, reject) {
        if (value) {
          resolve();
        } else {
          // resolve with text to show error message
          resolve(locText("field1_emptyInput"));
        }
      });
    }
  }).then(function(name) {
    if (name.dismiss) {
      // swal cancelled, so do nothing
    } else {
      // try to create the room
      MTLG.distributedDisplays.rooms.createRoom(name.value, function(result) {
        if (result && result.success) {
          callback(name.value);
        } else {
          swal.fire({
            type: 'error',
            title: locText("field1_addRoomErrorTitle"),
            text: result.reason,
            focusConfirm: true
          });
        }
      });
    }
  }, function(error) {
    swal.fire({
      type: 'error',
      title: locText("field1_addRoomErrorTitle"),
      text: error,
      focusConfirm: true
    });
  });
};


/**
 * This function fetches all available rooms.
 *
 * @return {Promise}  returns a promise
 */
var refreshRooms = function() {
  return new Promise(function(resolve, reject) {
    return MTLG.distributedDisplays.rooms.getAllRooms(function(result) {
      if (result && !(result.success === false)) {
        resolve(result);
      } else {
        reject(result);
      }
    });
  });
}


/**
 * This function updates the coordinates of the this object.
 *
 * @param  {type} {localX new x coordinate
 * @param  {type} localY} new y coordinate
 */
var moveCard = function({localX,localY}) {
  this.set(this.localToLocal(localX, localY, this.parent));
}

var colors = ['red', 'green', 'blue', 'yellow'];
var values = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];


/**
 * This function creates a card with a random color and random value.
 *
 * @return {createjs.Container}  Gives back a container looking like a card
 */
var getNewCard = function() {
  var settings = MTLG.getSettings().default;
  var color = colors[Math.floor(Math.random() * colors.length)];
  var value = values[Math.floor(Math.random() * values.length)];
  var container = new createjs.Container();
  let text = MTLG.utils.gfx.getText(value, settings.cards_height / 2 + 'px Arial', 'black');
  let textMargin = 9 / 10; // Margin relative to sizeX
  text.maxWidth = settings.cards_width * textMargin;
  text.x = settings.cards_width / 2; //Since regX is half the size
  text.y = settings.cards_height / 2 + text.getBounds().height / 4;
  text.regX = text.getBounds().width / 2;
  text.textBaseline = 'alphabetic';
  text.name = "text";

  var card = new createjs.Shape();
  card.graphics.beginStroke('black').beginFill(color).drawRect(0, 0, settings.cards_width, settings.cards_height);
  card.x = 0;
  card.y = 0;
  card.name = "background";

  container.on('pressmove', moveCard);
  container.addChild(card);
  container.addChild(text);
  container.regX = settings.cards_width / 2;
  container.regY = settings.cards_height / 2;
  container.x = MTLG.getOptions().width / 2;
  container.y = MTLG.getOptions().height / 2;
  container.mouseChildren = false;

  // make SharedObject because all cards should be sendable.
  new MTLG.distributedDisplays.SharedObject(container);

  // Workaround: Without this hitArea the "hitArea" of the container after sending is (on the sending side) on position 0 0 and not the real position.
  container.hitArea = card;

  return container;
}

/**
 * var createLinkedArea - This function adds a labelled linkedArea to the stage.
 *
 * @param  {number} x        x-coordinate
 * @param  {number} y        y-coordinate
 * @param  {number} width    width of the linkedArea
 * @param  {number} height   height of the linkedArea
 * @param  {type} receiver ClientId of the receiver of the linkedArea
 * @param  {String} text     tedt which should be written in the linkedArea
 * @return {Object}          {linkedArea: areaObject, text: textObject}
 */
var createLinkedArea = function(x, y, width, height, receiver, text) {
  var stage = MTLG.getStageContainer();
  var text = new createjs.Text(text, height / 8 + 'px Arial', 'black');
  text.x = x + 5;
  text.y = y + 5;
  stage.addChild(text);
  return {
    linkedArea: new MTLG.distributedDisplays.LinkedArea(x, y, width, height, true, receiver, false, true),
    text: text
  };
};


/**
 * removeLinkedArea - This function removes the linkedArea which was generate
 * with the function createLinkedArea
 *
 * @param  {type} area (return type of function createLinkedArea) LinkedArea and text-Object
 */
var removeLinkedArea = function (area) {
  area.linkedArea.delete();
  MTLG.getStageContainer().removeChild(area.text);
};

var utils = {
  rooms: {
    addRoom: addRoom,
    refreshRooms: refreshRooms
  },
  cards: {
    moveCard: moveCard,
    getNewCard: getNewCard
  },
  createLinkedArea: createLinkedArea,
  removeLinkedArea: removeLinkedArea
}

module.exports = {
  rooms: {
    addRoom: addRoom,
    refreshRooms: refreshRooms
  },
  cards: {
    moveCard: moveCard,
    getNewCard: getNewCard
  },
  createLinkedArea: createLinkedArea,
  removeLinkedArea: removeLinkedArea
}
