/**
 * @Date:   2019-03-19T10:14:00+01:00
 * @Last modified time: 2019-05-29T13:08:43+02:00
 */

var locText = MTLG.lang.getString;

var drawMainMenu = function(){

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
      console.log("Logging in");
      MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  }

  //go to qrDemo, if joinRoom is a parameter of the url (Scanned with QRCode)
  if (location.search.indexOf('joinRoom') !== -1) {
    MTLG.lc.levelFinished({
      nextLevel: "qrcode"
    });
  }


  var stage = MTLG.getStageContainer();

  let gWidth = MTLG.getOptions().width, gHeight = MTLG.getOptions().height;

  // Draw the main menu

  // White Background Rectangle
  var background = new createjs.Shape();
  background.graphics.beginFill('white').drawRect(0, 0, gWidth, gHeight);
  coordinates = {xCoord : gWidth / 2, yCoord : gHeight/2};
  background.regX = gWidth / 2;
  background.regY = gHeight / 2;
  background.x = coordinates.xCoord;
  background.y = coordinates.yCoord;

  // Text
  var variableTextSize = 50;
  var title = new createjs.Text(locText('menu_title'), variableTextSize + 'px Arial', 'black');
  var coordinates = {xCoord : gWidth / 2, yCoord : gHeight/4};
  title.x = coordinates.xCoord;
  title.y = coordinates.yCoord;
  title.regX = title.getBounds().width/2;
  title.regY = title.getBounds().height/2;
  title.textBaseline = 'alphabetic';

  // Buttons
  var basic = MTLG.utils.uiElements.addButton({
    text: locText("basic"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'red'
  }, function(){
    MTLG.lc.levelFinished({
      nextLevel: "basic"
    });
  });
  basic.x =  MTLG.getOptions().width / 2;
  basic.y = MTLG.getOptions().height / 3;

  var linkedAreaDemo = MTLG.utils.uiElements.addButton({
    text: locText("linkedAreaDemo"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'red'
  }, function(){
    MTLG.lc.levelFinished({
      nextLevel: "linkedAreaDemo"
    });
  });
  linkedAreaDemo.x =  MTLG.getOptions().width / 2;
  linkedAreaDemo.y = MTLG.getOptions().height / 3 + 100;

  var masterSlaves = MTLG.utils.uiElements.addButton({
    text: locText("masterSlaves"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'red'
  }, function(){
    MTLG.lc.levelFinished({
      nextLevel: "masterSlaves"
    });
  });
  masterSlaves.x =  MTLG.getOptions().width / 2;
  masterSlaves.y = MTLG.getOptions().height / 3 + 200;

  var dotGame = MTLG.utils.uiElements.addButton({
    text: locText("dotGame"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'red'
  }, function(){
    MTLG.lc.levelFinished({
      nextLevel: "dotGame"
    });
  });
  dotGame.x =  MTLG.getOptions().width / 2;
  dotGame.y = MTLG.getOptions().height / 3 + 300;

  var qrcode = MTLG.utils.uiElements.addButton({
    text: locText("qrcode"),
    sizeX: gWidth * 0.2,
    sizeY: 70,
    textColor: 'black',
    bgColor1: 'white',
    bgColor2: 'red'
  }, function(){
    MTLG.lc.levelFinished({
      nextLevel: "qrcode"
    });
  });
  qrcode.x =  MTLG.getOptions().width / 2;
  qrcode.y = MTLG.getOptions().height / 3 + 400;


  stage.addChild(background);
  stage.addChild(basic);
  stage.addChild(linkedAreaDemo);
  stage.addChild(masterSlaves);
  stage.addChild(dotGame);
  stage.addChild(qrcode);
  stage.addChild(title);
}

module.exports = {
  drawMainMenu : drawMainMenu
};
