/**
 * @Date:   2017-11-13T13:57:54+01:00
 * @Last modified time: 2019-05-29T11:10:06+02:00
 */


 MTLG.lang.define({
   'en': {
     'master' : 'Master',
     'slave' : 'Slave',
     'menu_title' : 'Menu',
     'basic' : 'Basic',
     'linkedAreaDemo': 'LinkedArea',
     'masterSlaves' : 'Master slave demo',
     'dotGame' : 'Game demo',
     'qrcode' : 'Join with QRCode',
     'back':'Back',

     // basic
     'request': 'Get Ball!',

     // linkedArea
     'la_f_f': 'copy: false, delete: false',
     'la_f_t': 'copy: false, delete: true',
     'la_t_f': 'copy: true, delete: false',
     'la_t_t': 'copy: true, delete: true',

     // masterSlaves
     'addCard': 'Add card',
     'otherPlayer': 'other Player',
     'addRoom': 'Add room',
     'refreshRooms': 'Refresh',
     'Oops...': 'Oops...',
     'errorMoreThan3': 'Not more than 3 clients allowed',
     'error': 'Error',
     'only_two_players': 'Only two players are allowed',
     'table': 'Table',
     'only_one_master': 'Only one master table is allowed',
     'playerButton': 'Player',
     'NoAnswer': 'No response! Something went wrong.',
     'Room not available': 'Room is not available',

     //dot game
     'field1_addRoomErrorTitle': 'Room creation failed',
     'field1_addRoomPlaceholder': 'Room name',
     'field1_addRoomQuestion': 'Please enter the name of the room to add:',
     'field1_addRoomTitle': 'Add a new room',
     'field1_emptyInput': 'You need to write something!',
     'field1_joinRoomButton': 'JOIN',
     'field1_joinRoomErrorTitle': 'Failed to join room',
     'field1_newRoomButton': 'NEW ROOM',
     'field1_refreshButton': 'REFRESH',
     'field1_title': 'Join an existing room or create a new one.',

     'field2_currentMembers': 'Current members',
     'field2_joinGameButton': 'JOIN',
     'field2_joinGameNote': 'This group is already playing, you can join them or leave the room.',
     'field2_leaveRoomButton': 'LEAVE ROOM',
     'field1_leaveRoomErrorTitle': 'Failed to leave room',
     'field2_memberControls_rotation': 'Rotation',
     'field2_memberControls_size': 'Size',
     'field2_memberControls_true': 'Nothing',
     'field2_memberControls_x': 'X-axis',
     'field2_memberControls_y': 'Y-axis',
     'field2_startButton': 'START',
     'field2_subTitle1': 'You',
     'field2_subTitle2': 'are a member of the room',
     'field2_title': 'Choose a control and start the game.',

     'field3_backToSelectControlsButton': 'SELECT CONTROL',
     'field3_gameOverTitle': 'Game Over',
     'field3_gameOverText': 'You have touched an obstacle',
     'field3_gameOverConfirm': 'START OVER',

     'general_fullscreenButton': 'FULLSCREEN'
   },
   'de': {
     'master' : 'Master',
     'slave' : 'Slave',
     'menu_title' : 'Menü',
     'basic' : 'Basic',
     'linkedAreaDemo': 'LinkedArea',
     'masterSlaves' : 'Demo Master Slave',
     'dotGame' : 'Demo eines Spiels',
     'qrcode' : 'QRCode Demo',
     'back': 'Zurück',

     // dot game
     'field1_addRoomErrorTitle': 'Raum konnte nicht erstellt werden',
     'field1_addRoomPlaceholder': 'Raum Name',
     'field1_addRoomQuestion': 'Bitte den Namen des zu erstellenden Raumes eingeben:',
     'field1_addRoomTitle': 'Neuen Raum erstellen',
     'field1_emptyInput': 'Das feld darf nicht leer sein!',
     'field1_joinRoomButton': 'BEITRETEN',
     'field1_joinRoomErrorTitle': 'Dem Raum konnte nicht beigetreten werden',
     'field1_refreshButton': 'AKTUALISIEREN',
     'field1_newRoomButton': 'RAUM HINZUFÜGEN',
     'field1_title': 'Trete einem Raum bei oder erstelle einen neuen.',

     'field2_currentMembers': 'Derzeitige Mitglieder',
     'field2_joinGameButton': 'SPIELEN',
     'field2_joinGameNote': 'Diese Gruppe spielt bereits, du kannst beitreten oder den Raum verlassen.',
     'field2_leaveRoomButton': 'RAUM VERLASSEN',
     'field1_leaveRoomErrorTitle': 'Der Raum konnte nicht verlassen werden',
     'field2_memberControls_rotation': 'Rotation',
     'field2_memberControls_size': 'Größe',
     'field2_memberControls_true': 'Nichts',
     'field2_memberControls_x': 'X-Achse',
     'field2_memberControls_y': 'Y-Achse',
     'field2_startButton': 'STARTEN',
     'field2_subTitle1': 'Du',
     'field2_subTitle2': 'bist ein Mitglied des Raumes',
     'field2_title': 'Wähle eine Steuerung und starte das Spiel.',

     'field3_backToSelectControlsButton': 'STEUERUNG WÄHLEN',
     'field3_gameOverTitle': 'Das Spiel ist aus',
     'field3_gameOverText': 'Du hast ein Hindernis berührt',
     'field3_gameOverConfirm': 'NEUSTART',

     'general_fullscreenButton': 'VOLLBILD'
   }
 });
