/**
 * @Date:   2019-03-07T14:05:39+01:00
 * @Last modified time: 2019-08-14T11:43:41+02:00
 */



MTLG.loadOptions({
  dd: {
    host: "https://ai.ddi.education/dd"
  },
  "width":1920, //game width in pixels
  "height":1080, //game height in pixels
  "languages":["en","de"], //Supported languages. First language should be the most complete.
  "countdown":180, //idle time countdown
  "fps":"60", //Frames per second
  "playernumber":4, //Maximum number of supported players
  "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  "webgl": false //Using webgl enables using more graphical effects
});
