/**
 * @Date:   2017-11-13T09:46:53+01:00
 * @Last modified time: 2019-08-14T11:33:07+02:00
 */



/**
 * Initialization of the distributed displays module
 * @author Aaron Grabowy
 * @namespace distributedDisplays
 * @memberof MTLG
 */

/* Private properties */

/**
 * Configuration
 * @private
 * @memberof MTLG.distributedDisplays#
 */
var config = {
    /** Which actions are performed if sender is not in the same room */
    allowFromOutside: {
        addSharedObjects: false,
        customAction: false,
        deleteSharedObject: false,
        updateSharedObject: false
    },
    /** Debug mode, more verbose output and all rights */
    debug: false,
    debug_requests_actions: false,
    /** Port to connect with, this has to be adjusted if process.env.PORT was used server side */
    port: 3125,
    /** Default to localhost */
    host: "http://localhost",
    /** Maximal buffer delay of subsequent updates in ms */
    maxUpdateDelay: 20
};

// Forward declare debug object
var debug = {};

var {getPropertiesTemplate, serialize, deserialize, getClassName, instantiateClass} = require('./scheme');
var LinkedArea = require('./linkedAreas').default;

/**
 * Time since last transmission of the updateBuffer
 * @private
 * @type {number}
 * @memberof MTLG.distributedDisplays#
 */
var lastUpdate = 0;

/**
 * Buffer for subsequent updates of the same property
 * @private
 * @type {Object.<string, object | boolean>}
 * @alias module:distributedDisplays.updateBuffer
 */
var updateBuffer = {};

/**
 * Socket.io object, either connected to the server or trying to
 * @private
 * @type Socket
 * @alias module:distributedDisplays.socket
 */
var socket;



/* Private functions */

/**
 * Do nothing, used as default callback
 * @private
 * @alias module:distributedDisplays.noop
 */
var noop = function () { };

/**
 * Perform the action specified in the action object, if the room setting allow this
 * @param {object} action The action to perform, see messageSchema.json
 * @param {string} from The name of the sender
 * @param {string} to The name of the receiver
 * @param {boolean} isInSameRoom True, if sender and receiver are in the same room
 * @private
 * @alias module:distributedDisplays.performAction
 */
var performAction = function (action, from, to, isInSameRoom) {
    if (!action.action) return console.error("rooms.performAction: No .action specified!");

    // The action is performed if the sender is in same room or if the config allows this action to be triggered from outside
    if (isInSameRoom || config.allowFromOutside[action.action]) {
        if(config.debug_requests_actions)
            console.log('%c'+(Date.now()-debug._startTime)+' REMOTE '+action.action+' '+{
                    "addSharedObjects": '',
                    "customAction": action.identifier,
                    "deleteSharedObject": (action.sharedId||'').slice(0,4),
                    "updateSharedObject": (action.sharedId||'').slice(0,4)+', '+action.propertyPath+': '+action.oldValue+' → '+action.newValue,
                }[action.action], 'color: #f0f')
        // console.log('%cbefore:', 'color: #aa0')
        // MTLG.distributedDisplays.debug.printLocalState()
        switch (action.action) {
            case "addSharedObjects": actions.addSharedObjects(action.toAdd, from); break;
            case "customAction": actions.customAction(action.identifier, action.parameters); break;
            case "deleteSharedObject": actions.deleteSharedObject(action.sharedId, from); break;
            case "updateSharedObject": actions.updateSharedObject(action.sharedId, action.propertyPath, action.oldValue, action.newValue, action.additionalInfo, from); break;
            default: MTLG.warn("ddd", "rooms.performAction: No action defined for ", action.action); break;
        }
        // console.log('%cafter:', 'color: #aa0')
        // MTLG.distributedDisplays.debug.printLocalState()
    } else {
        MTLG.log("ddd", "rooms.performAction: Action from ", from, " was not performed, as he is not in the same room as ", to, ".", action);
    }
};

/**
 * Private helper to send a request to the server, arguments are:
 * @param {string} command The event name to emit
 * @param {...any} varArgs Varying amount of parameters to send, the last one is the callback function
 * @param {function} callback The last parameter is the callback function
 * Use .bind(this) or something similar to set the desired value for "this" inside the callback
 * @private
 * @alias module:distributedDisplays.sendRequest
 */
var sendRequest = function (command, ...args) {
    // Check if callback is a function
    if (arguments.length < 2) return console.error("rooms.sendRequest: Number of parameters (", arguments.length, ") is less than 2.");
    var callback = arguments[arguments.length - 1];
    if (typeof callback !== 'function') {
        console.error("rooms.sendRequest: Please provide a callback function as last argument. Original call was ", command);
        return;
    }

    // Check if connected to server
    if (!socket.connected) {
        console.error("rooms.sendRequest: Trying to perform " + command + ", but Socket is not connected!");
        callback({ success: false, reason: "No connection", command: command, arguments: arguments });
        return;
    }

    if(config.debug_requests_actions) {
        let description = () => { switch(command) {
            case 'sendMessage': switch(args[0].action) {
                    case "addSharedObjects": return args[0].action
                    case "customAction": return args[1].action+" " + args[0]
                    case "deleteSharedObject": return args[0].action+" " + (args[0].sharedId||'').slice(0,4)
                    default: return args[0].action
                }
            case "sendUpdate": return (args[0]||'').slice(0,4)+', '+args[1]+': '+args[2]+' → '+args[3]
            case "registerUpdateListener":
            case "deregisterUpdateListener":
                return (args[0]||'').slice(0,4)
        } }
        console.log('%c'+(Date.now()-debug._startTime)+' LOCAL '+command+' '+description(), 'color: #0ff')
    }

    // Send the request itself by calling socket.emit() with the arguments passed to sendRequest(). (apply(valueForThis, listOfArguments))
    socket.emit.apply(socket, arguments);
};

/**
 * Transmits all buffered updates
 * @param {object} event The update event
 * @private
 * @alias module:distributedDisplays.sendUpdateBuffer
 */
var sendUpdateBuffer = function (event) {
    lastUpdate += event.delta;
    if (lastUpdate >= config.maxUpdateDelay) { // If delayed enough
        var u;
        for (var i in updateBuffer) {
            // Send all buffered updates
            if (updateBuffer[i]) {
                u = updateBuffer[i];
                if (u.isPlaceholder) {
                    // Only placeholder, clear entry
                    updateBuffer[i] = false;
                } else {
                    // Send buffered update
                    sendRequest('sendUpdate', u.sharedId, u.propertyPath, u.oldValue, u.newValue, u.additionalInfo, noop);

                    // Convert entry to placeholder
                    u.isPlaceholder = true;
                    u.oldValue = u.newValue; // Setup oldValue for future buffer entries
                    u.additionalInfo = undefined; // Clear additionalInfo
                }
            }
        }
        lastUpdate = 0;
    }
};



/* Public functions */

/*
 * Initializes the distributed displays module
 */
var init = function (options) {
    MTLG.log("ddd", "Initializing distributed displays module");
    createjs.Ticker.addEventListener("tick", sendUpdateBuffer); // Steady transmission of the updateBuffer
    /** Url of the server */
//    config.url = "192.168.178.54:" + config.port + "/";
    Object.assign(config, options.dd);
    config.url = config.host + ":" + config.port + "/";

    socket = io(config.url); // Connect with server

    // Received text to log
    socket.on('log', function (message) {
        MTLG.log("ddd", "log: ", message);
    });

    /**
     * Received a message from another client
     * @param {string} from The identifier of the message's sender
     * @param {string} to The name of the receiver
     * @param {boolean} isInSameRoom True, if sender and receiver are in the same room
     * @param {Message | Message[]} content The received message
     * @private
     * @alias module:distributedDisplays.receiveMessage
     */
    var receiveMessage = function (from, to, isInSameRoom, content) {
        if (Array.isArray(content)) {
            // Content is a list of actions
            for (var i = 0; i < content.length; i++) {
                performAction(content[i], from, to, isInSameRoom);
            }
        } else {
            // Content is a single action
            performAction(content, from, to, isInSameRoom);
        }
    };

    socket.on('receiveMessage', receiveMessage);


      /**
       * This function returns an array with all key value pairs of the url parameters
       *
       * @param  {String} query String from url after "?"
       * @return {object}       Array of key value pairs.
       */
      var parse_query_string = function (query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split("=");
          var key = decodeURIComponent(pair[0]);
          var value = decodeURIComponent(pair[1]);
          // If first entry with this name
          if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
          }
        }
        return query_string;
      }

      // get parameters from url
      var params = parse_query_string(window.location.search.substring(1));
      socket.on('connect', function () {
        // if the parameter joinRoom is given, the client should join the room.
        if(params['joinRoom']) {
          var roomName = params['joinRoom'];
          rooms.joinRoom(roomName, function(result) {
            if(!result.success) {
              // TODO: What happens if the room cannot be joined?
              console.error('Error while joining room ' + roomName, result);
            }else{
              MTLG.log("dd", 'Joined room "'+ roomName + '"successfully');
            }
          });
        }
      }, null, true);

    // Log socket.io events for debugging
    if (config.debug) {

        config.allowFromOutside.addSharedObjects = true;
        config.allowFromOutside.customAction = true;
        config.allowFromOutside.deleteSharedObject = true;
        config.allowFromOutside.updateSharedObject = true;

        // Successfully connected
        socket.on('connect', function () {
            MTLG.log("ddd", "Connected to distributed displays server with id ", socket.id);
        });
        // Not connected or disconnected
        socket.on('connect_error', function (err) {
            MTLG.warn("ddd", "socket.connect_error: ", err);
        });
        socket.on('connect_timeout', function () {
            MTLG.warn("ddd", "socket.connect_timeout");
        });
        socket.on('disconnect', function () {
            MTLG.warn("ddd", "socket.disconnect: Connection to distributed displays server lost!");
        });
        socket.on('error', function (err) {
            MTLG.warn("ddd", "socket.error: ", err);
        });
    }
};


/*- Rooms (first layer) -*/


/**
 * Room management.
 * @namespace rooms
 * @memberof MTLG.distributedDisplays
 */

let rooms = {

    /**
     * Creates a new room with the given name, fails if a room or client with that name already exists
     * @param {string} name The name of the new room
     * @param {function} callback Callback to call with object as only parameter: {success: boolean, name: string, reason?: string, details?: object}
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     */
    createRoom: function(name, callback) {
        // Checking if room or client already exists happens server side
        sendRequest('createRoom', name, callback);
    },

    /**
     * Gets a list of all clients
     * @param {function} callback Callback to call with a list of the clients as only parameter.
     * The list is an Array of strings.
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     */
    getAllClients: function(callback) {
        sendRequest('getAllClients', callback);
    },

    /**
     * Gets a list of all rooms
     * @param {function} callback Callback to call with a list of the rooms as only parameter.
     * An example list is:<pre><code>{room1: {
     *     length: 2,
     *     sockets: {ewEZ7oegtR4vz6sfAAAB: true, iJmIfL7025vAZtpBAAAD: true}
     * }, room2: {
     *     length: 1,
     *     sockets: {iJmIfL7025vAZtpBAAAD: true}
     * }}
     * </code></pre>
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     */
    getAllRooms: function(callback) {
        sendRequest('getAllRooms', callback);
    },

    /**
     * Gets the id of this client
     * @returns {string} The id of this client
     * @memberof MTLG.distributedDisplays.rooms#
     */
    getClientId: function() {
        return socket.id;
    },

    /**
     * Gets a list of all joined rooms
     * @param {function} callback Callback to call with a list of the joined rooms as only parameter.
     * An example list is:<pre><code>{room1: {
     *     length: 2,
     *     sockets: {ewEZ7oegtR4vz6sfAAAB: true, iJmIfL7025vAZtpBAAAD: true}
     * }}
     * </code></pre>
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     */
    getJoinedRooms: function(callback) {
        sendRequest('getJoinedRooms', callback);
    },

    /**
     * Join an existing room with the given name, fails if no room with that name exists
     * @param {string} name The name of the room to join
     * @param {function} callback Callback to call with object as only parameter: {success: boolean, name: string, reason?: string, details?: object}
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     */
    joinRoom: function(name, callback) {
        // Checking if room exists happens server side
        sendRequest('joinRoom', name, callback);
    },

    /**
     * Leave an existing room with the given name, fails if no room with that name exists
     * @param {string} name The name of the room to leave
     * @param {function} callback Callback to call with object as only parameter: {success: boolean, name: string, reason?: string, details?: object}
     * Use .bind(this) or something similar to set the desired value for "this" inside the callback
     * @memberof MTLG.distributedDisplays.rooms#
     *
     */
    leaveRoom: function(name, callback) {
        // Checking if room exists happens server side
        sendRequest('leaveRoom', name, callback);
    }
}

/*- Data transfer (second layer) -*/

/**
 * Data transfer between the devices and convenience functions for actions. Class with only static methods.
 * @namespace communication
 * @memberof MTLG.distributedDisplays
 */

let communication = {

    /**
     * Remove this socket from the list of listeners for updates of the SharedObject with the given sharedId
     * @param {string} sharedId The sharedId of the SharedObject to stop listening for
     * @memberof MTLG.distributedDisplays.communication#
     */
    deregisterUpdateListener: function(sharedId) {
        sendRequest('deregisterUpdateListener', sharedId, undefined, noop); // Undefined means the server should remove this socket from the list of listeners.
        // The second server side parameter can be used to specify a different client to deregister (if not undefined), but is needed in order to have the callback at the right position
    },

    /**
     * Register this socket to listen for updates of the SharedObject with the given sharedId
     * @param {string} sharedId The sharedId of the SharedObject to keep synchronized
     * @memberof MTLG.distributedDisplays.communication#
     */
    registerUpdateListener: function(sharedId) {
        sendRequest('registerUpdateListener', sharedId, undefined, noop);
    },

    /**
     * Send an update of the SharedObject with the given sharedId to the server
     * @param {string} sharedId The sharedId of the changed SharedObject
     * @param {string} propertyPath The path from the createJsObject (excluding) to the changed property (including)
     * @param {any} oldValue The old value of the property
     * @param {any} newValue The new value of the property
     * @param {object} [additionalInfo] Additional information, that is transmitted as is.
     * E.g. additionalInfo.addHelpers is used to transmit SharedObject helpers, that are needed for the newValue.
     * @memberof MTLG.distributedDisplays.communication#
     */
    sendUpdate: function(sharedId, propertyPath, oldValue, newValue, additionalInfo) {
        var id = sharedId + ":" + propertyPath;
        // If this property was already updated before the minimum update-delay expired, the current update is delayed.
        // An exception are updates that may set a SharedObject because the remote must register the SharedObject before
        // it can receive any updates on the object. Every local update before registering is lost for the remote otherwise.
        if (updateBuffer[id] && !(additionalInfo && additionalInfo.className == "SharedObject")) {
            updateBuffer[id].newValue = newValue; // Store the newValue to transmit with the next update bulk
            updateBuffer[id].additionalInfo = additionalInfo;
            updateBuffer[id].isPlaceholder = false;
        } else {
            // No update for this property is buffered, transmit it without delay
            sendRequest('sendUpdate', sharedId, propertyPath, oldValue, newValue, additionalInfo, noop);

            // Add placeholder to delay subsequent updates
            updateBuffer[id] = {
                isPlaceholder: true,
                sharedId: sharedId,
                propertyPath: propertyPath,
                oldValue: newValue, // Set for future subsequent updates
                newValue: newValue
                // AdditionalInfo doesn't have to be buffered
            };
        }
    },

    /**
     * Send a message to a room or client, nothing happens if the receiver does not exist
     * @param {string} to The client or room name to send to
     * @param {any} content The content to sent, has to be an object conforming the messageSchema.json
     * @memberof MTLG.distributedDisplays.communication#
     */
    sendMessage: function(to, content) {
        sendRequest('sendMessage', to, content, noop);
    },


    /*- Message creation (third layer) -*/

    /**
     * Send a message that triggers a call of the function registered under the identifier. All other parameters are passed to the function.
     * @param {string} to The client or room name to send to
     * @param {string} identifier The identifier of the custom function to call
     * @param {...any} parameters The parameters to call the custom function with
     * @memberof MTLG.distributedDisplays.communication#
     */
    sendCustomAction: function(to, identifier, ...parameters) {
        try {
            communication.sendMessage(to, {
                action: "customAction",
                identifier: identifier,
                parameters: parameters
            });
        } catch (err) {
            console.error("communication.sendCustomAction: Failed to transmit the custom actions. Please ensure that the parameters", parameters, "are serializable.", err);
        }
    },

    /**
     * Sends the given SharedObject to the receiver using the addSharedObjects action
     * @param {string} to The client or room name to send to
     * @param {SharedObject} sharedObject The SharedObject to send
     * @param {boolean} [independentCopyThere] If true, the SharedIds of the sendables are changed, so the receiver has a copy that is not synchronized with the original
     * @param {boolean} [deleteHere] If true, the SharedObject and the wrapped CreateJS object is removed after the transmission
     * @memberof MTLG.distributedDisplays.communication#
     */
    sendSharedObject: function(to, sharedObject, independentCopyThere = false, deleteHere = false) {
        // Send the SharedObject to the receiver
        communication.sendMessage(to, {
            action: "addSharedObjects",
            toAdd: sharedObject.toSendables(independentCopyThere)
        });
        // Delete the SharedObject on this side (sender)
        if (deleteHere) sharedObject.delete(true, false); // delete(removeCreateJsObject, skipGarbageCollection)
    },

    /**
     * Sends a message that triggers the deletion of the SharedObject on all receivers
     * @param {string} to The client or room name to send to
     * @param {string} sharedId The sharedId of the SharedObject to delete
     * @memberof MTLG.distributedDisplays.communication#
     */
    sendSharedObjectDeletion: function(to, sharedId) {
        communication.sendMessage(to, {
            action: "deleteSharedObject",
            sharedId: sharedId
        });
    }
}



/******************** Actions ********************/


/**
 * Parsing messages and performing actions for distributed displays module
 * @namespace actions
 * @memberof MTLG.distributedDisplays
 */

/* Public functions */


/*- Actions -*/

let actions = {

    /* Private properties */

    /**
     * A set of functions to call after an action has been executed, is stored here for each action
     * @private
     * @type {Object.<string, Set>}
     * @alias module:actions.actionListeners
     */
    actionListeners: {
        addSharedObjects: new Set(),
        deleteSharedObject: new Set(),
        updateSharedObject: new Set()
    },

    /**
     * The functions to call for the customAction are stored here, the identifier of the custom action is used as key
     * @private
     * @type {Map}
     * @alias module:actions.customActionFunctions
     */
    customActionFunctions: new Map(),


    /* Public methods */

    /**
     * Adds the given SharedObjects to the list of SharedObjects and draws them in the referenced Container
     * @param {Object.<string, Sendable>} toAdd Associative Array containing the Sendable versions of the SharedObjects to add
     * @param {string} from The name of the sender of the action
     * @memberof MTLG.distributedDisplays.actions#
     */
    addSharedObjects: function(toAdd, from) {
        var skip = {}, added = {};

        // First phase: Recreate objects without links to other SharedObject
        for (var i in toAdd) {
            if (SharedObject.getSharedObject(i) !== undefined) {
                skip[i] = true;

                if (!toAdd[i].isHelper) MTLG.warn("ddd", "actions.addSharedObjects: SharedObject with sharedId ", i, " already exists");
            }

            // Recreate the createJsObject, init is postponed until all SharedObjects to add are created
            if (!skip[i]) {
                added[i] = SharedObject.fromSendable(toAdd[i], true);
            }
        }

        // Second phase: Calculate coordinates and add to parent / stage
        for (var i in added) {
            if (!skip[i]) {
                // Finish initialization
                added[i].init();

                // Add to visible stage
                if (added[i].parent == "Stage")
                    MTLG.getStage().addChild(added[i].createJsObject);
                if (added[i].parent == "StageContainer")
                    MTLG.getStageContainer().addChild(added[i].createJsObject);
            }
        }

        // Call listeners
        var param = {
            action: "addSharedObjects",
            toAdd: toAdd,
            added: added,
            from: from
        };
        actions.actionListeners.addSharedObjects.forEach(function (listener) {
            listener(param);
        });
    },

    /**
     * Calls the custom function registered under the identifier with the given parameters
     * @param {string} identifier The identifier of the custom function to call
     * @param {Array} parameters The parameters to pass to the custom function
     * @memberof MTLG.distributedDisplays.actions#
     */
    customAction: function(identifier, parameters) {
        var customFunc = actions.customActionFunctions.get(identifier);
        if (customFunc) {
            customFunc.apply(window, parameters);
        } else {
            MTLG.warn("ddd", "actions.customAction: No custom is function registered under the identifier", identifier);
        }
    },

    /**
     * Delete the SharedObject with the given sharedId
     * @param {string} sharedId The sharedId of the SharedObject to delete
     * @param {string} from The name of the sender of the action
     * @memberof MTLG.distributedDisplays.actions#
     */
    deleteSharedObject: function(sharedId, from) {
        var s = SharedObject.getSharedObject(sharedId);
        if (s) {
            s.delete(true, false); // delete(removeCreateJsObject, skipGarbageCollection)

            // Call listeners
            var param = {
                action: "deleteSharedObject",
                sharedId: sharedId,
                from: from
            };
            actions.actionListeners.deleteSharedObject.forEach(function (listener) {
                listener(param);
            });
        }
    },

    /**
     * Update the SharedObject with the given sharedId
     * @param {string} sharedId The sharedId of the SharedObject to update
     * @param {string} propertyPath The path to the property to change
     * @param {any} oldValue The old value of the property (old value from the sender). Can be set to undefined or null, but not omitted
     * @param {any} newValue The new value of the property
     * @param {object} additionalInfo Additional information, that is transmitted as is.
     * E.g. additionalInfo.addHelpers is used to transmit SharedObject helpers, that are needed for the newValue.
     * @param {string} from The name of the sender of the action
     * @memberof MTLG.distributedDisplays.actions#
     */
    updateSharedObject: function(sharedId, propertyPath, oldValue, newValue, additionalInfo, from) {
        if(propertyPath.endsWith('length')) console.error('THIS SHOULDN\'T HAPPEN! length IS ABANDONED!!')

        // Check if the SharedObject exists
        var sharedObj = SharedObject.getSharedObject(sharedId);
        if (sharedObj) {
            additionalInfo = additionalInfo || {};
            // Retrieve entry for the property in the syncProperties tree
            var props = propertyPath.split(".");
            var lastProp = props[props.length - 1];
            var lastPropSync = lastProp; // lastPropSync is for changing the lastProp for syncParent when working with Arrays
            var syncParent = sharedObj.syncProperties;
            var sharedParent = sharedObj.createJsObject;
            var sharedGrandParent;
            var itemClass; // Class name of an item inside an Array, used to decide how to step further into syncProperties from Array.itemClasses
            for (var i = 0; i < props.length - 1; i++) {
                if (syncParent.hasOwnProperty(props[i]) && sharedParent.hasOwnProperty(props[i])) {

                    sharedGrandParent = sharedParent;
                    sharedParent = sharedParent[props[i]]; // One step further down the propertyPath for sharedParent regardless of the className

                    if (syncParent[props[i]].className === "Array") {
                        syncParent = syncParent[props[i]].itemClasses; // One step for the syncParent
                        // Check the type of the item in the Array
                        if (i < props.length - 2) { // The item, which is the next in the propertyPath is already stored in this SharedObject
                            // Perform another step to get the item
                            i++; // Skips the next loop iteration
                            sharedGrandParent = sharedParent;
                            sharedParent = sharedParent[props[i]]; // sharedParent was the Array, the item is located at index props[i] and is the new sharedParent, second step for sharedParent
                            itemClass = getClassName(sharedParent); // Class name of the item
                            // Second step for syncParent
                            if (syncParent.hasOwnProperty(itemClass)) syncParent = syncParent[itemClass].subProperties; // Either options for a specific class of items
                            else if (syncParent.hasOwnProperty("other")) {
                                // Only options for general other items can be applied, so other.send decides about the remaining propertyPath
                                lastPropSync = "other"; // syncParent["other"].send will be checked in the next step
                                i = props.length; // End stepping into syncProperties here as other has no further subProperties or itemClasses
                            } else return; // Or stop here as syncProperties does not include the propertyPath
                        } else { // The item, which is the next in the propertyPath is the newValue and is not yet stored in this SharedObject
                            if (lastProp != "length") { // Special handling of the length property skips the next block
                                itemClass = additionalInfo.className; // Class name of the just set item, stored separately for Array items
                                lastPropSync = syncParent.hasOwnProperty(itemClass) ? itemClass : "other"; // Modify lastProp so that checking syncParent[lastProp].send gives the correct result in the next step
                            }
                        }
                    } else { // No Array

                        // Set the new syncParent for the usual case
                        syncParent = syncParent[props[i]].subProperties;

                    }
                } else return; // Stop here as the full propertyPath cannot be walked down
            }

            // Update the property if receiving updates for this property is set and (the SharedObject has this property or its parent is an Array)
            if (syncParent[lastPropSync] && syncParent[lastPropSync].receive && (sharedParent.hasOwnProperty(lastProp) || getClassName(sharedParent) === "Array")) {
                var opts = syncParent[lastPropSync].className ? syncParent[lastPropSync] : {
                    send: syncParent[lastPropSync].send,
                    receive: syncParent[lastPropSync].receive,
                    className: additionalInfo.className
                };
                // Indicating different old values might help for future debugging
                if (config.debug && sharedParent[lastProp] !== oldValue) {
                    if (opts.className === "cache" || opts.className === "style" || (sharedParent[lastProp] === undefined && oldValue === null) || (sharedParent[lastProp] === null && oldValue === undefined)) {
                        // Different old values for cacheID are common and make no difference
                        // Undefined can become null while transmitting with socket.io
                    } else {
                        MTLG.warn("ddd", "actions.updateSharedObject: Old values are not the same", sharedParent[lastProp], oldValue);
                    }
                }

                // Handle additional information
                if (additionalInfo) {
                    // Recreate additional helpers if needed
                    if (additionalInfo.addHelpers) {
                        actions.addSharedObjects(additionalInfo.addHelpers);
                    }

                    // Handle deletion
                    if (additionalInfo.delete) {
                        // Store what will be deleted to prevent calling onChange
                        sharedObj.justReceived = {
                            propertyPath: propertyPath,
                            newValue: "delete"
                        };
                        delete sharedParent[lastProp];
                    }
                }

                if (!additionalInfo || !additionalInfo.delete) { // Regular updating

                    // Workaround for direct changes of Fill and Strokes style.props object
                    if (opts.className === "styleProps") {
                        opts.sharedGrandParent = sharedGrandParent;
                    }

                    // Convert value from Sendable
                    var newVal = deserialize(newValue, opts, sharedObj);

                    // Store what will be updated to prevent calling onChange
                    sharedObj.justReceived = {
                        propertyPath: propertyPath,
                        newValue: newVal
                    };

                    // Update the property
                    sharedParent[lastProp] = newVal; // The value, which is already converted from a Sendable, is finally assigned to the property
                }

                // Call listeners
                var param = {
                    action: "updateSharedObject",
                    sharedId: sharedId,
                    propertyPath: propertyPath,
                    oldValue: oldValue,
                    newValue: newVal,
                    additionalInfo: additionalInfo,
                    from: from
                };
                actions.actionListeners.updateSharedObject.forEach(function (listener) {
                    listener(param);
                });
            }
        } else {
            MTLG.warn("ddd", "actions.updateSharedObject: Received update for a not existing SharedObject.", sharedId, propertyPath, oldValue, newValue);
        }
    },


    /*- Listeners -*/

    /**
     * Removes the given function from the list of listeners for the given action
     * @param {string} actionName The name of the action to deregister the listener from. Possible values are "addSharedObjects", "deleteSharedObject", and "updateSharedObject".
     * @param {function} listener The function to deregister
     * @memberof MTLG.distributedDisplays.actions#
     */
    deregisterListenerForAction: function(actionName, listener) {
        if (actions.actionListeners.hasOwnProperty(actionName)) {
            actions.actionListeners[actionName].delete(listener);
        } else {
            MTLG.warn("ddd", "actions.deregisterListenerForAction: There is no list of listeners for the action", actionName, ".");
        }
    },

    /**
     * Register the listener function to be called when the given action has been executed
     * @param {string} actionName The name of the action that triggers the function call. Possible values are "addSharedObjects", "deleteSharedObject", and "updateSharedObject".
     * @param {function} listener The function to call when the action has been executed.
     * The function is always called with an object as only parameter, but the properties of that object depend on the action:<ul>
     * <li>addSharedObjects: {action: "addSharedObjects", toAdd: {[sharedId: string]: sendable}, added: {[sharedId: string]: SharedObject}, from: string}</li>
     * <li>deleteSharedObject: {action: "deleteSharedObject", sharedId: string, from: string}</li>
     * <li>updateSharedObject: {action: "updateSharedObject", sharedId: string, propertyPath: string, oldValue: any, newValue: any, additionalInfo: object, from: string}</li></ul>
     * Use .bind(this) or something similar to set the desired value for "this" inside the listener function
     * @memberof MTLG.distributedDisplays.actions#
     */
    registerListenerForAction: function(actionName, listener) {
        if (typeof listener !== 'function')
            return console.error("actions.registerListenerForAction: Please provide a function as second argument.");

        if (actions.actionListeners.hasOwnProperty(actionName)) {
            actions.actionListeners[actionName].add(listener);
        } else {
            MTLG.warn("ddd", "actions.registerListenerForAction: There is no list of listeners for the action", actionName, ".");
        }
    },

    /**
     * Removes the custom function for the given identifier. A customAction with that identifier will no longer invoke a function call
     * @param {string} identifier The identifier of the custom function to remove
     * @memberof MTLG.distributedDisplays.actions#
     */
    removeCustomFunction: function(identifier) {
        actions.customActionFunctions.delete(identifier);
    },

    /**
     * Defines the function that is called when a customAction with the given identifier is executed
     * @param {string} identifier The identifier of the customAction that triggers the function call
     * @param {function} customFunction The function that is called when a customAction with the given identifier is executed
     * @memberof MTLG.distributedDisplays.actions#
     */
    setCustomFunction: function(identifier, customFunction) {
        actions.customActionFunctions.set(identifier, customFunction);
    }
}







/******************** SharedObject ********************/

/*
* SharedObject class for the distributed displays module
*/

/**
 * A SharedObject is a wrapper class for CreateJS objects, which stores additional information to synchronize the CreateJS object across devices.
 * @memberof MTLG.distributedDisplays#
 */
class SharedObject {

    /**
     * Creates a new SharedObject containing the given CreateJS object
     * @param {object} createJsObject The CreateJS object to wrap
     * @param {boolean} [isHelper] True, if this SharedObject is created automatically to represent a property of another SharedObject.
     * @param {boolean} [postponeInit] Set this to true to skip the initialization (defining helpers and setup of watchers). You have to manually call init(), if set to true.
     * @param {string} [sharedId] Set this optional sharedId to recreate a SharedObject with the given sharedId, omit it to create a new SharedObject.
     */
    constructor (createJsObject, isHelper = false, postponeInit = false, sharedId) {
        /** The sharedId of this SharedObject
         * @type {string} */
        this.sharedId = sharedId || getNextId();
        // Add to list of SharedObjects
        registerLocalSharedObject(this);

        /** The wrapped CreateJS object
         * @type {createjs.DisplayObject} */
        this.createJsObject = createJsObject;

        /** List of SharedIds, that are used as a helper
         * @type {Set.<string>} */
        this.helpers = new Set();

        /** Is this SharedObject created to represent the property of another SharedObject
         * @type {boolean} */
        this.isHelper = isHelper === true ? true : false;





        /** Store what was just received to prevent calling onChanged when the property is updated
         * @type {{propertyPath: string, newValue: any}} */
        this.justReceived = {
            propertyPath: "",
            newValue: null
        }

        /** Defines which properties sync their changes with the server
         * @type {object} */
        this.syncProperties = getPropertiesTemplate();

        // Mark the createJsObject as synchronized over this SharedObject
        this.createJsObject.sharedId = this.sharedId;




        // Initialize helpers and watchers
        if (!postponeInit) this.init();
    }




    /* Public methods */

    /**
     * Removes the SharedObject and the wrapped CreateJS object
     * @param {boolean} [removeCreateJsObject] If true, the wrapped CreateJS object is removed from the Stage as well
     * @param {boolean} [skipGarbageCollection] If this optional parameter is true, the subsequent garbage collection is skipped.
     * This might be useful to prevent multiple runs of the garbage collection when deleting multiple SharedObjects.
     */
    delete (removeCreateJsObject = false, skipGarbageCollection = false) {
        communication.deregisterUpdateListener(this.sharedId); // Stop listening for updates
        //if (removeCreateJsObject && this.parent == 'Stage' && this.createJsObject.parent)  this.createJsObject.parent.removeChild(this.createJsObject); // Remove createJsObject from the stage
        if (removeCreateJsObject && this.createJsObject.parent) this.createJsObject.parent.removeChild(this.createJsObject); // Remove createJsObject from the stage
        sharedObjects.delete(this.sharedId); // Remove from list of SharedObjects
        this.createJsObject = null; // Remove reference to wrapped object and leave the rest to JavaScripts garbage collector
        if (!skipGarbageCollection) garbageCollectHelpers(); // Delete helpers, that are no longer needed
    }

    /**
     * Creates helpers for SharedObject properties and sets up watchers according to this.syncProperties.
     * This is called automatically unless postponeInit is set in the constructor.
     */
    init () {
        // Create SharedObjects as helper for properties of type SharedObject
        for (var p in this.syncProperties) {
            if (this.createJsObject.hasOwnProperty(p) && this.createJsObject[p] !== null && this.syncProperties[p].className && this.syncProperties[p].className === "SharedObject") {
                // Create a new helper if not already existing
                getOrCreateHelper(this.createJsObject[p], false, this);
            }
        }

        // Watch for changes
        watchProperties(this.createJsObject, this.syncProperties, "", this, onChange);
    }

    /**
     * Returns a sendable representation of this SharedObject, but helpers are just referenced and not completely included in the return value.
     * This method is for internal use. Use toSendables to serialize a SharedObject for transmission.
     * @returns {Sendable} A sendable representation of this SharedObject.
     */
    sendable () {
        // Store general information
        var res = {
            createJsObject: {}, // Contains the values of all syncedProperties
            sharedId: this.sharedId, // The sharedId of this SharedObject
            isHelper: this.isHelper, // Is this SharedObject created to represent the property of another SharedObject
            syncProperties: this.syncProperties, // Defines which properties sync their changes with the server
            className: getClassName(this.createJsObject) // The class name of the createJsObject
        }

        // Store parent
        if (this.createJsObject.parent && this.createJsObject.parent.sharedId) res.parent = this.createJsObject.parent.sharedId;
        else {
            switch (this.createJsObject.parent) {
                case MTLG.getStage():
                    res.parent = "Stage";
                    break;
                case MTLG.getStageContainer():
                    res.parent = "StageContainer";
                    this.parent = "StageContainer";
                    break;
                case undefined:
                case null:
                    res.parent = "None";
                    break;
                default:
                    MTLG.warn("ddd", "SharedObjec.sendable: No behavior defined for parent", this.createJsObject.parent);
                    res.parent = "None";
                    break;
            }
        }

        // Store properties
        for (var p in this.syncProperties) {
            if (this.createJsObject.hasOwnProperty(p)) {
                res.createJsObject[p] = serialize(this.createJsObject[p], this.syncProperties[p], this);
            }
        }

        return res;
    }

    /**
     * Returns an associative Array containing the sendable versions (serializable) of this SharedObject and all helpers needed to recreate this SharedObject.
     * Use this method to transmit a SharedObject.
     * @param {boolean} [independentCopy] If true, new SharedIds are used to create a copy that is not synchronized with the original
     * @returns {Object.<string, Sendable>} The associative Array containing the sendables of this SharedObject and all helpers needed to recreate this SharedObject.
     */
    toSendables (independentCopy = false) {
        garbageCollectHelpers(); // Remove all unneeded helpers from the helpers list
        var res = {}; // Associative Array containing the sendables of the given SharedObject and all helpers
        // Convert sharedObject to sendable
        res[this.sharedId] = this.sendable();
        res[this.sharedId].isRoot = true; // Mark the root of the sendables, the others are only helpers
        // Convert helpers to sendable and add to list
        var helpers = new Set(this.helpers); // var helpers contains the ids of SharedObjects, that are still to convert
        while (helpers.size !== 0) { // While there are still helpers to convert,
            for (let helperId of helpers) { // iterate over them and convert
                if (!res[helperId]) { // Helper is not already converted
                    var helper = SharedObject.getSharedObject(helperId); // Get the SharedObject from the sharedId
                    res[helperId] = helper.sendable(); // Convert to sendable and add to list
                    for (let i of helper.helpers) { // Add the helpers of the helper to the list of SharedObjects to convert,
                        if (!res[i]) { // if they are not already converted
                            helpers.add(i);
                        }
                    }
                }
                helpers.delete(helperId); // Remove converted helper from the list
            }
        }

        // Change the SharedIds to stop the synchronization
        if (independentCopy) {
            // Create new SharedIds, store the sendables at the new location and fill the idMap
            var newId,
                idMap = {},
                oldRes = res;
            res = {};
            for (var oldId in oldRes) {
                if (oldRes.hasOwnProperty(oldId)) {
                    newId = getNextId(); // Retrieve a new sharedId
                    idMap[oldId] = newId; // Save the relation from the old to the new sharedId
                    res[newId] = oldRes[oldId]; // Store sendable at the new location
                    res[newId].sharedId = newId; // Update the sharedId
                    res[newId].createJsObject.sharedId = newId; // Update the sharedId
                }
            }
            // Recursively change the references to other SharedObjects once the idMap is filled
            for (var i in res) {
                if (res.hasOwnProperty(i)) {
                    changeSharedIds(res[i].createJsObject, res[i].syncProperties, idMap);
                }
            }
        }

        return res;
    }




    /* Public static methods */

    /**
     * Recreates the SharedObject represented by the given Sendable.
     * @param {Sendable} sendable The Sendable representation of the SharedObject to recreate
     * @param {boolean} [postponeInit] Set this to true to skip the initialization (defining helpers and setup of watchers). You have to manually call init().
     * @returns {SharedObject} The recreated SharedObject.
     */
    static fromSendable (sendable, postponeInit = false) {
        // Only the properties are created and the values are set here.
        // Watchers and links to other SharedObjects are added in init().

        // Recreate base objects of the correct type
        var resCreate = instantiateClass(sendable.className);
        var resShared = new SharedObject(resCreate, sendable.isHelper, true, sendable.sharedId); // postponeInit is always true here, the parameter only influences the init() call at the end
        // Store general information
        resShared.parent = sendable.parent;
        resShared.syncProperties = sendable.syncProperties;

        // Recreate properties
        for (var p in sendable.syncProperties) {
            // Convert the property and store the value
            resCreate[p] = deserialize(sendable.createJsObject[p], sendable.syncProperties[p], resShared);
        }

        // Update cache if needed, has to be done after filters are recreated
        if (resCreate.cacheID) { // !== 0 means cache is enabled
            resCreate.updateCache();
        }

        // Initialize helpers and watchers
        if (!postponeInit) resShared.init();
        return resShared;
    }

    /**
     * Returns the SharedObject with the given sharedId
     * @param {string} sharedId The sharedId of the SharedObject to return
     * @returns {SharedObject} The SharedObject with the given sharedId
     */
    static getSharedObject (sharedId) {
        return sharedObjects.get(sharedId);
    }
}




/* Private functions */

/**
 * Changes the references to other SharedObjects to the new SharedId
 * @param {object} baseProp Sendable property to start the recursive traversing from
 * @param {object} syncProps Matching (to baseProp) syncProperties entry to start the recursive traversing from
 * @param {Object.<string, string>} idMap Object that maps from the old SharedIds to the new SharedIds
 * @private
 * @alias SharedObject.changeSharedIds
 */
var changeSharedIds = function (baseProp, syncProps, idMap) {
    for (var p in syncProps) {
        if (baseProp.hasOwnProperty(p) && baseProp[p]) {
            if (syncProps[p].className === "SharedObject") { // Reference to a SharedObject
                baseProp[p] = idMap[baseProp[p]]; // Change the reference to the new SharedId
            } else if (syncProps[p].className === "Array" && syncProps[p].itemClasses.hasOwnProperty("SharedObject")) { // Array of SharedObjects, other Array types do not contain references
                for (var i in baseProp[p]) {
                    if (baseProp[p][i] && i !== "length") {
                        baseProp[p][i] = idMap[baseProp[p][i]]; // Change the reference to the new SharedId
                    }
                }
            }

            // Recursively go through subproperties
            if (syncProps[p].subProperties) {
                changeSharedIds(baseProp[p], syncProps[p].subProperties, idMap);
            }
        }
    }
};


/**
 * Check which helpers are still referenced by SharedObjects and release the unneeded for garbage collection.
 * @private
 * @alias SharedObject.garbageCollectHelpers
 */
var garbageCollectHelpers = function () {
    // Reset tags
    for (var sharedObj of sharedObjects.values()) {
        sharedObj.isNeeded = false; // Is the object referenced by another SharedObject. Also used to mark already processed objects, so they are not checked a second time.
        sharedObj.helpers.clear(); // Clear the list of helpers, which gets filled again in the next step.
    }

    // Update helpers and isNeeded using depth first traversal with non helpers as children of the root
    for (var sharedObj of sharedObjects.values()) {
        if (!sharedObj.isHelper) {
            gcUpdateHelpers(sharedObj);
        }
    }

    // Delete reference to unneeded helpers and release them for garbage collection
    for (var sharedObj of sharedObjects.values()) {
        if (sharedObj.isHelper && !sharedObj.isNeeded) {
            sharedObj.delete(false, true); // delete(removeCreateJsObject, skipGarbageCollection)
        }
    }
};

/**
 * Private helper function for garbageCollectHelpers.
 * Updates the .isNeeded and .helpers property of the given SharedObject
 * @param {SharedObject} sharedObj The SharedObject to update
 * @param {object} [baseProp] Optional subproperty of the SharedObject to use as basis for iterating. Used to traverse through subproperties recursively.
 * syncProps has to be set as well if used.
 * @param {object} [syncProps] Optional list of properties to iterate over. Used to traverse through subproperties recursively.
 * baseProp has to be set as well if used.
 * @private
 * @alias SharedObject.gcUpdateHelpers
 */
var gcUpdateHelpers = function (sharedObj, baseProp, syncProps) {
    if (syncProps || !sharedObj.isNeeded) { // Do not check a SharedObject a second time. This check is skipped for subproperties in order to make recursive calls for subproperties possible.
        sharedObj.isNeeded = true; // Every SharedObject, that is visited can be reached from a manually created SharedObject and is therefore still needed
        baseProp = baseProp || sharedObj.createJsObject; // Use either the SharedObject itself or the given subproperty of the SharedObject
        syncProps = syncProps || sharedObj.syncProperties; // Use either the base .syncProperties of the SharedObject or the given subset of subProperties
        for (var p in syncProps) {
            // Check if the SharedObject has the property p and its value is neither null nor undefined
            if (baseProp.hasOwnProperty(p) && baseProp[p]) {

                // Mark referenced SharedObjects as needed and update their helpers recursively
                if (syncProps[p].className === "SharedObject") {
                    // Property p is of type SharedObject, the parent of p has the property and its value is neither null nor undefined
                    let helper = SharedObject.getSharedObject(baseProp[p].sharedId);
                    sharedObj.helpers.add(helper.sharedId); // Add the referenced helper to sharedObj's list of needed helpers
                    gcUpdateHelpers(helper); // Process the referenced helper and mark it as needed
                } else if (syncProps[p].className === "Array") {
                    if (syncProps[p].itemClasses.hasOwnProperty("SharedObject")) { // Special handling for Arrays of SharedObjects
                        for (var i in baseProp[p]) {
                            if (baseProp[p][i] && i !== "length") {
                                let helper = SharedObject.getSharedObject(baseProp[p][i].sharedId);
                                sharedObj.helpers.add(helper.sharedId);
                                gcUpdateHelpers(helper);
                            }
                        }
                    } // Other Array types do not contain references to other SharedObjects
                }

                // Recursively go through subproperties
                if (syncProps[p].subProperties) {
                    gcUpdateHelpers(sharedObj, baseProp[p], syncProps[p].subProperties);
                }
            }
        }
    }
};

/**
 * Retrieves an existing helper if value is a SharedId or an object with a .sharedId property or creates a new helper monitoring the value
 * @param {string | {sharedId: string} | object} value A SharedId or object with .sharedId of an existing helper or object to monitor by a new helper
 * @param {boolean} postponeInit The postponeInit value to use, if a new helper is created
 * @param {SharedObject} helperFor The SharedObject this helper is needed for. The helper is added to the helpers list of the given SharedObject.
 * @returns {SharedObject} The retrieved or created helper
 * @private
 * @alias SharedObject.getOrCreateHelper
 */
var getOrCreateHelper = function (value, postponeInit, helperFor) {
    // First check if value is a SharedId of an existing helper
    var helper = SharedObject.getSharedObject(value);
    // Second check if value is a CreateJs object, that is already monitored
    if (!helper) helper = SharedObject.getSharedObject(value.sharedId); // value.sharedId can be undefined, then helper is undefined too
    // If no helper exists, create a new helper
    if (!helper) helper = new SharedObject(value, true, postponeInit);
    // Add helper to the list of helpers
    helperFor.helpers.add(helper.sharedId);
    return helper;
};

/**
 * Returns a unique sharedId
 * @returns {string} A unique sharedId
 * @private
 * @alias SharedObject.getNextId
 */
var getNextId = function () {
    idCounter++;
    return (idCounter + Math.random()) + "";
};


/**
 * Handles changes of a SharedObject.
 * If the property is listed under this.syncProperties and .send is true, then the change is sent to the server
 * @param {string} propertyPath The changed property, written as property.subproperty for deeper elements
 * @param {any} oldValue The old value of the property
 * @param {any} newValue The new value of the property
 * @param {{className: string, subProperties: object}} options Options influencing the sendable version of the new value, all of its properties are optional
 * @param {object} [additionalInfo] Additional information, that is transmitted as is.
 * E.g. additionalInfo.addHelpers is used to transmit SharedObject helpers, that are needed for the newValue.
 * @private
 * @alias SharedObject.onChange
 */
var onChange = function (propertyPath, oldValue, newValue, options, additionalInfo) {
    if(propertyPath.endsWith('length')) console.error('REMOVE ME!!')

    // If the SharedObject has moved, the LinkedAreas are notified
    if (propertyPath === "x" || propertyPath === "y" || propertyPath === "regX" || propertyPath === "regY") {
        LinkedArea.handleMovement(this);
    }

    // Retrieve the syncProperties entry and optionally className
    if (newValue !== oldValue) {
        additionalInfo = additionalInfo || {};
        // Retrieve entry for the property in the syncProperties tree
        var props = propertyPath.split(".");
        var lastProp = props[props.length - 1];
        var syncParent = this.syncProperties;
        var sharedParent = this.createJsObject;
        var itemClass; // Class name of an item inside an Array, used to decide how to step further into syncProperties from Array.itemClasses
        for (var i = 0; i < props.length - 1; i++) {
            if (syncParent.hasOwnProperty(props[i]) && sharedParent.hasOwnProperty(props[i])) {

                sharedParent = sharedParent[props[i]]; // One step further down the propertyPath for sharedParent regardless of the className

                if (syncParent[props[i]].className === "Array") {
                    syncParent = syncParent[props[i]].itemClasses; // One step for the syncParent
                    // Check the type of the item in the Array
                    if (i < props.length - 2) { // The item, which is the next in the propertyPath is already stored in this SharedObject
                        // Perform another step to get the item
                        i++; // Skips the next loop iteration
                        sharedParent = sharedParent[props[i]]; // sharedParent was the Array, the item is located at index props[i] and is the new sharedParent, second step for sharedParent
                        itemClass = getClassName(sharedParent); // Class name of the item
                        // Second step for syncParent
                        if (syncParent.hasOwnProperty(itemClass)) syncParent = syncParent[itemClass].subProperties; // Either options for a specific class of items
                        else if (syncParent.hasOwnProperty("other")) {
                            // Only options for general other items can be applied, so other.send decides about the remaining propertyPath
                            lastProp = "other"; // syncParent["other"].send will be checked in the next step
                            i = props.length; // End stepping into syncProperties here as other has no further subProperties or itemClasses
                            additionalInfo.className = additionalInfo.className || getClassName(newValue); // Save the class of the new property value
                            options.className = options.className || getClassName(newValue);
                        } else return; // Or stop here as syncProperties does not include the propertyPath
                    } else { // The item, which is the next in the propertyPath is the newValue and is not yet stored in this SharedObject
                        if (lastProp != "length") { // Special handling of the length property skips the next block
                            itemClass = getClassName(newValue); // Class name of the just set item
                            if (options.className && options.className === "SharedObject") itemClass = "SharedObject"; // Special handling for Arrays of SharedObjects
                            lastProp = syncParent.hasOwnProperty(itemClass) ? itemClass : "other"; // Modify lastProp so that checking syncParent[lastProp].send gives the correct result in the next step
                            additionalInfo.className = additionalInfo.className || itemClass; // Save the class of the new item
                            options.className = options.className || itemClass;
                        }
                    }
                } else { // No Array

                    // Set the new syncParent for the usual case
                    syncParent = syncParent[props[i]].subProperties;

                }
            } else return; // Stop here as the full propertyPath cannot be walked down
        }

        // Check if update shall be sent
        if (syncParent[lastProp] && syncParent[lastProp].send) {
            let newVal = additionalInfo.delete ? "delete" : serialize(newValue, options, this);
            communication.sendUpdate(this.sharedId, propertyPath, oldValue, newVal, additionalInfo);
        }
    }
};

/**
 * Handler for the Array Proxy. Intercepts calls to the created Proxy. Used to share Arrays.
 * @param {string} propertyPath The path to the Proxy
 * @param {object} options The .itemClasses options for the proxied Array
 * @param {SharedObject} valueForThis The value to be used for "this" inside the callback. Reference to the SharedObject, where the proxy is a member of.
 * @param {function} callback The callback, that is called when the property changes. Called with callback(propertyPath, oldValue, newValue, options, [additionalInfo])
 * @returns {ProxyHandler<Array>} The ProxyHandler to intercept calls to the proxy
 * @private
 * @alias SharedObject.proxyArray
 */
var proxyArray = function (propertyPath, options, valueForThis, callback) {
    return {
        deleteProperty: function (target, property) {
            if (!(property in target))
                return false; // Property to delete not found (undefined is a valid property)

            let propPath = propertyPath + "." + property;

            if (options && options["SharedObject"]) { // Special handling for Arrays of SharedObjects
                // Call the onChanged callback, if the change was not triggered by receiving an update
                if ((valueForThis.justReceived.propertyPath !== propPath || valueForThis.justReceived.newValue !== "delete") && !valueForThis.justReceived.ignoreAll) {
                    let adInfo = { delete: true, className: "SharedObject" };
                    callback.call(valueForThis, propPath, target[property], "delete", options["SharedObject"], adInfo); // = callback(propertyPath, oldValue, newValue, options, additionalInfo) with valueForThis as this
                }
            } else { // Array without SharedObjects
                // Call the onChanged callback, if the change was not triggered by receiving an update
                if ((valueForThis.justReceived.propertyPath !== propPath || valueForThis.justReceived.newValue !== "delete") && !valueForThis.justReceived.ignoreAll) {
                    let opts = {};
                    let adInfo = { delete: true };
                    if (target[property] !== undefined && target[property] !== null) {
                        adInfo.className = getClassName(target[property]);
                        opts = (options && options[adInfo.className]) ? options[adInfo.className] : { className: adInfo.className };
                    }
                    callback.call(valueForThis, propPath, target[property], "delete", opts, adInfo); // = callback(propertyPath, oldValue, newValue, options, additionalInfo) with valueForThis as this
                }
            }

            // To avoid issues with 'empty' elements every deletion is treated as pop()
            target.pop();

            return true;
        },

        get: function (target, property, receiver) {
            // length is treated special and never shared because it has side-effects on the other properties
            if(property == "length") return target.length;

            if(options && options["SharedObject"] && SharedObject.getSharedObject(target[property]))
                // Dereference children that are SharedObjects
                return SharedObject.getSharedObject(target[property]).createJsObject;
            // else if(options && options["SharedObject"] && typeof target[property] == "string")
            //     // easeljs may ask for an object even if it is not stage anymore, provide a dummy in the of a deleted sharedObject
            //     return {isVisible:()=>false, sharedId:target[property], _debug:'This sharedObject is already deleted.'};
            else
                // Return stored value if no SharedObject type or no SharedObject was found
                return target[property];
        },

        set: function (target, property, value, receiver) {
            // length is treated special and never shared because it has side-effects on the other properties
            if(property == "length") {
                target.length = value;
                return true;
            }

            let propPath = propertyPath + "." + property;

            if (options && options["SharedObject"]) { // Special handling for Arrays of SharedObjects
                let oldVal = target[property];
                let newVal, helper, additionalInfo;
                if (value && typeof value !== "string") {
                    helper = getOrCreateHelper(value, false, valueForThis);
                    newVal = helper.sharedId;
                } else {
                    // Take value as it is if null, undefined or a SharedId. getOrCreate a helper otherwise
                    newVal = value;
                    if (newVal && typeof newVal === "string") valueForThis.helpers.add(newVal);
                }

                // Call the onChanged callback, if the change was not triggered by receiving an update
                if ((valueForThis.justReceived.propertyPath !== propPath || valueForThis.justReceived.newValue !== value) && !valueForThis.justReceived.ignoreAll) {
                    target[property] = newVal; // Needed here before helper.toSendables() to prevent GC from removing the helper
                    if (helper) additionalInfo = { addHelpers: helper.toSendables() }; // Transmit helper
                    callback.call(valueForThis, propPath, oldVal, newVal, options["SharedObject"], additionalInfo); // = callback(propertyPath, oldValue, newValue, options, additionalInfo) with valueForThis as this
                } else {
                    target[property] = newVal;
                }


            } else { // Array without SharedObjects
                let opts = (options && value && options[getClassName(value)]) ? options[getClassName(value)] : {};

                // Call the onChanged callback, if the change was not triggered by receiving an update
                if ((valueForThis.justReceived.propertyPath !== propPath || valueForThis.justReceived.newValue !== value) && !valueForThis.justReceived.ignoreAll) {
                    callback.call(valueForThis, propPath, target[property], value, opts); // = callback(propertyPath, oldValue, newValue, options) with valueForThis as this
                }

                target[property] = value; // Set the new value

                if (opts && opts.subProperties) { // Options for that class of item are defined and the new value is neither undefined nor null
                    watchProperties(target[property], opts.subProperties, propPath, valueForThis, callback);
                }
            }
            return true;
        }
    }
};

/**
 * Add the given SharedObject to the list of sharedObjects and register the listener for updates
 * @param {SharedObject} toAdd The SharedObject to register
 * @private
 * @alias SharedObject.registerLocalSharedObject
 */
var registerLocalSharedObject = function (toAdd) {
    // Add to list of SharedObjects
    sharedObjects.set(toAdd.sharedId, toAdd);
    // Listen for updates
    communication.registerUpdateListener(toAdd.sharedId);
};

/**
 * Private helper function to define a callback, that is triggered every time one of the listed properties of the object toWatch changes.
 * @param {object} toWatch The object to watch, parent of the listed properties
 * @param {object} properties The properties of the object, that trigger the callback
 * @param {string} pathPrefix A prefix to prepend before the property, used to build the propertyPath for callback
 * @param {object} valueForThis The value to be used for "this" inside the callback
 * @param {function} callback The callback, that is called when the property changes. Called with callback(propertyPath, oldValue, newValue, options, [additionalInfo])
 * @private
 * @alias SharedObject.watchProperties
 */
var watchProperties = function (toWatch, properties, pathPrefix, valueForThis, callback) {
    if (!toWatch || typeof toWatch !== "object") return; // toWatch has to be defined and an object

    // For all properties listed in properties
    for (let p in properties) { // let is needed here
        let propPath = pathPrefix === "" ? p : pathPrefix + "." + p;
        // Watch the property p
        watchProperty(toWatch, p, propPath, properties[p], valueForThis, callback);

        // Recursively watch subproperties of p
        if (properties[p].subProperties) watchProperties(toWatch[p], properties[p].subProperties, propPath, valueForThis, callback);
    }
};

/**
 * Private helper function to define a callback, that is triggered every time the property of the parent object changes.
 * @param {object} parent The parent, which has the property to watch
 * @param {string} property The property to watch
 * @param {string} propertyPath Path from the SharedObject (excluding) to the property to watch (including), written as property.subproperty for deeper elements
 * @param {{className: string, subProperties: object}} options Watchers for subproperties in this list are added when the property is overridden.
 * Can be null or undefined if not needed.
 * The className influences the sendable version of the property
 * @param {object} valueForThis The value to be used for "this" inside the callback
 * @param {function} callback The callback, that is called when the property changes. Called with callback(propertyPath, oldValue, newValue, options, [additionalInfo])
 * @private
 * @alias SharedObject.watchProperty
 */
var watchProperty = function (parent, property, propertyPath, options, valueForThis, callback) {
    switch (options.className) {
        // Special handling for SharedObjects: Store the SharedId to reference helpers. Retrieve helper for getter and set / create helper for setter. Trigger GarbageCollection and onChange.
        case "SharedObject":
            // Store the sharedId of the referenced helper locally
            let sharedId;
            if (parent[property]) {
                if (typeof parent[property] === "string") sharedId = parent[property]; // SharedId is stored directly in the property, e.g. after converting from Sendable
                else if (parent[property].sharedId) sharedId = parent[property].sharedId; // The property is already monitored by a SharedObject, so that sharedId is used
                else sharedId = null; // No suitable sharedId found
            } else {
                // No SharedObject helper set (e.g. value is null or undefined)
                sharedId = null;
            }
            // Define a getter and setter for the SharedObject helper
            Object.defineProperty(parent, property, {
                configurable: true,
                get: function () {
                    var shared = SharedObject.getSharedObject(sharedId);
                    return shared ? shared.createJsObject : null; // Return either the createJsObject of the referenced SharedObject or null
                },
                set: function (value) {
                    var additionalInfo, oldValue = sharedId;
                    if (value) {
                        // Value is neither null nor undefined, so a helper can be used to represent it
                        var helper = getOrCreateHelper(value, false, valueForThis);
                        // Store new sharedId
                        sharedId = helper.sharedId;
                        additionalInfo = { addHelpers: helper.toSendables() };
                    } else {
                        sharedId = null;
                        // Old helper cannot simply be removed from .helpers as the helper might be used for a different property as well
                    }

                    garbageCollectHelpers(); // Perform garbage collection of helpers. This could be done after calling the callback to improve performance (usually only a few ms), but the old helper would unnecessarily be transmitted as well

                    // Call the onChanged callback, if the change was not triggered by receiving an update
                    if ((valueForThis.justReceived.propertyPath !== propertyPath || valueForThis.justReceived.newValue !== value) && !valueForThis.justReceived.ignoreAll) {
                        callback.call(valueForThis, propertyPath, oldValue, sharedId, options, additionalInfo); // = callback(propertyPath, oldValue, newValue, options, additionalInfo) with valueForThis as this
                    }

                    return value; // Return what a normal setter would return
                }
            });
            break;


        // Special handling of Arrays: Use Proxy to replace the Array
        case "Array":

            let proxy;
            let oldArray = parent[property];
            // Define a getter and setter for the property
            Object.defineProperty(parent, property, {
                configurable: true,
                get: function () {
                    return proxy;
                },
                set: function (value) {
                    // Call the onChanged callback, if the change was not triggered by receiving an update
                    if ((valueForThis.justReceived.propertyPath !== propertyPath || valueForThis.justReceived.newValue !== value) && !valueForThis.justReceived.ignoreAll) {
                        callback.call(valueForThis, propertyPath, proxy, value, options); // = callback(propertyPath, oldValue, newValue, options) with valueForThis as this
                    }
                    // Create a Proxy for the new value
                    proxy = value ? new Proxy(new value.constructor(), proxyArray(propertyPath, options.itemClasses, valueForThis, callback)) : value; // Empty proxy of the same type
                    // Set all values
                    for (let p in value) { // Set up watchers for subProperties of items if the item's class is defined in itemClasses
                        if (value.hasOwnProperty(p)) { // Checking if value[p] would exclude array entries like false or 0
                            // prevent sending the changes when setting up the proxy in the next line
                            valueForThis.justReceived = {
                                propertyPath: propertyPath + "." + p,
                                newValue: value[p]
                            };
                            // following line won't trigger a send-request
                            proxy[p] = value[p]; // Watching subProperties is done in the set proxyArray
                        }
                    }
                    // reset justReceived
                    valueForThis.justReceived = {
                        propertyPath: "",
                        newValue: null
                    }
                    return proxy; // Return what a normal setter would return
                }
            });
            // Trigger the just defined setter for the new value
            valueForThis.justReceived = {
                propertyPath: propertyPath,
                newValue: oldArray
            };
            parent[property] = oldArray;
            break;


        // Usual handling for other property types: Use setter to trigger onChange and set up listeners for subProperties
        default:
            // Store the value locally
            let storedValue = parent[property];
            // Define a getter and setter for the property
            Object.defineProperty(parent, property, {
                configurable: true, // Needs to be reconfigurable so that another call of init can be run
                get: function () {
                    return storedValue;
                },
                set: function (value) {
                    // Call the onChanged callback, if the change was not triggered by receiving an update
                    if ((valueForThis.justReceived.propertyPath !== propertyPath || valueForThis.justReceived.newValue !== value) && !valueForThis.justReceived.ignoreAll) {
                        callback.call(valueForThis, propertyPath, storedValue, value, options); // = callback(propertyPath, oldValue, newValue, options) with valueForThis as this
                    }
                    storedValue = value; // Set the new value
                    if (options.subProperties && value) { // List of subproperties is defined and the new value is neither undefined nor null
                        watchProperties(storedValue, options.subProperties, propertyPath, valueForThis, callback);
                    }
                    return storedValue; // Return what a normal setter would return
                }
            });
            break;
    }
};


/**
 * This value is incremented to give each SharedObject a unique sharedId
 * @private
 * @type {number}
 * @alias SharedObject.idCounter
 */
var idCounter = 0;

/**
 * Associative array where all SharedObjects are stored by their sharedId
 * @private
 * @type {Map.<string, SharedObject>}
 * @alias SharedObject.sharedObjects
 */
var sharedObjects = new Map();

/* functions useful in debugging */


/**
 * @name debug
 * @memberof MTLG.distributedDisplays#
 * @property {function} printLocalState Prints all shared object-ids and all dependent shared objects (=helpers).
 * @property {object} state Provides access to all shared objects.
 * @property {function} garbageCollectHelpers Runs the module's garbage collector.
 * @property {function} snapshot Returns a human readable snapshot of the current state that can be compared with a snapshot at another point in time.
 */
Object.assign(debug, {
    /* align to 1min intervals for better comparison between clients */
    _startTime: Math.round(Date.now() /60/1000) *60*1000,
    _log: false,
    printLocalState: function() {
        let print = (so, indent='', printed=[]) => {
            let label = createJsObject => {
                switch(createJsObject.constructor.name) {
                    case 'Container': return '#'+createJsObject.children.length
                    case 'Shape': return JSON.stringify(createJsObject.graphics.command)
                    case 'Object': return Object.keys(createJsObject).join(',')
                    default: return ''
                }
            }
            if(!printed.includes(so.sharedId)) printed.push(so.sharedId)
            let out = indent
            if(!so) return out + 'undefined'
            out += so.isHelper ? '/' : ''
            out += so.sharedId.slice(0,4) + ' '
            out += so.createJsObject.constructor.name + ' '
            out += label(so.createJsObject)
            out += '\n'
            for(let key of so.helpers) out += print(sharedObjects.get(key), indent+' ', printed)
            return out
        }

        let out = '', printed = []
        for(let [_, so] of sharedObjects) out += so.isHelper && printed.includes(so.sharedId)
            ? '_' + so.sharedId.slice(0,4) + '\n'
            : print(so, '', printed)
        console.log(out)
    },
    state: { sharedObjects },
    garbageCollectHelpers,
    snapshot: function() {
        let res = {};
        let scheme = getPropertiesTemplate();
        for(so of sharedObjects.values()) {
            let jso = res[so.sharedId.slice(0,4)] = {};
            for (var key in scheme) {
                if (so.createJsObject.hasOwnProperty(key)) {
                    jso[key] = serialize(so.createJsObject[key], scheme[key] || {}, so)
                }
            }
        }
        return res;
    }
});

// Reveal public methods to the module
if (!MTLG.distributedDisplays) MTLG.distributedDisplays = {};
Object.assign(MTLG.distributedDisplays, {
    init,
    rooms,
    communication,
    actions,
    SharedObject,
    LinkedArea,
    debug
});
MTLG.addModule(init);
