/**
 * @Date:   2019-03-19T14:40:34+01:00
 * @Last modified time: 2019-04-24T16:33:00+02:00
 */



/*
* LinkedArea class for the distributed displays module
*/

/**
 * A LinkedArea sends SharedObjects, that are dragged inside the LinkedArea, to another client.
 * @alias LinkedArea
 */
class LinkedArea {

    /**
     * Creates a new LinkedArea
     * @param {number} x The x-coordinate of the upper left corner of the LinkedArea
     * @param {number} y The y-coordinate of the upper left corner of the LinkedArea
     * @param {number} w The width of the LinkedArea
     * @param {number} h The height of the LinkedArea
     * @param {boolean} visible If true, a rectangular Shape is painted with the extends of the LinkedArea
     * @param {string} receiver The client or room name to send the entered SharedObject to
     * @param {boolean} [independentCopyThere] If true, the receiver gets a copy that is not synchronized with the entered SharedObject
     * @param {boolean} [deleteHere] If true, the entered SharedObject and CreateJS object is removed after the transmission
     */
    constructor (x, y, w, h, visible, receiver, independentCopyThere = false, deleteHere = false) {
        /** The configuration for the appearance */
        this.config = {
            fill: "#00549F25",
            stroke: "#00549FF7",
            hoverFill: "#8FB9DF25",
            hoverStroke: "#8FB9DFF7"
        };

        /** The visible appearance of the LinkedArea, shown if visible is true
         * @type {createjs.Shape} */
        this.appearance = new createjs.Shape();
        this.appearance.graphics.beginFill(this.config.fill).beginStroke(this.config.stroke).drawRect(x, y, w, h);
        this.appearance.visible = visible;
        MTLG.getStageContainer().addChild(this.appearance);

        /** The coordinates and size of the LinkedArea
         * @type {createjs.Graphics.Rect} */
        this.rect = this.appearance.graphics.command;

        /** The client or room name to send the entered SharedObject to
         * @type {string} */
        this.receiver = receiver;

        /** If true, the receiver gets a copy that is not synchronized with the entered SharedObject
         * @type {boolean} */
        this.independentCopyThere = independentCopyThere;

        /** If true, the entered SharedObject and CreateJS object is removed after the transmission
         * @type {boolean} */
        this.deleteHere = deleteHere;

        /** A Set of sharedIds of all SharedObjects that are inside of this LinkedArea
         * @type {Set.<string>} */
        this.sharedIdsInside = new Set();

        // Add this LinkedArea to the Set of all LinkedAreas
        linkedAreas.add(this);

        // Register a custom function for received SharedObjects if not already done
        if (!initialized) {
            MTLG.distributedDisplays.actions.registerListenerForAction("addSharedObjects", receivedSharedObjectHandler);
            initialized = true;
        }
    }




    /* Public methods */

    /**
     * Removes the LinkedArea
     */
    delete () {
        if (this.appearance.parent) this.appearance.parent.removeChild(this.appearance); // Remove visible appearance from the stage
        linkedAreas.delete(this); // Remove from list of LinkedAreas
        this.appearance = null; // Remove reference to appearance and leave the rest to JavaScripts garbage collector
    }




    /* Public static methods */

    /**
     * Handles the movement of the given SharedObject by checking if it is inside of any LinkedArea
     * @param {SharedObject} sharedObj The moved SharedObject to handle
     */
    static handleMovement(sharedObj) {
        // Register pressup handler if not already done
        if (!pressupRegistered.has(sharedObj)) {
            sharedObj.createJsObject.addEventListener("pressup", pressupHandler);
            pressupRegistered.add(sharedObj);
        }

        // Get the origin of the sharedObj in world (stage) coordinates
        var pos = sharedObj.createJsObject.localToGlobal(sharedObj.createJsObject.regX, sharedObj.createJsObject.regY);
        for (var l of linkedAreas) {
            // Check if the sharedObj is inside or outside for each LinkedArea
            if (isInside(l, pos)) {
                l.sharedIdsInside.add(sharedObj.sharedId);
                // Update appearance to hover state
                l.appearance.graphics._fill.style = l.config.hoverFill;
                l.appearance.graphics._stroke.style = l.config.hoverStroke;
            } else {
                l.sharedIdsInside.delete(sharedObj.sharedId);
                if (l.sharedIdsInside.size === 0) {
                    // Update appearance to normal state
                    l.appearance.graphics._fill.style = l.config.fill;
                    l.appearance.graphics._stroke.style = l.config.stroke;
                }
            }
        }
    }
}




/* Private functions */

/**
 * This functions checks if the given position is inside or outside of the
 * given linkedArea.
 * @param {LinkedArea} linkedArea -
 * @param {object} pos - Position, that should be checked
 * @param {number} pos.x - x coordinate
 * @param {number} pos.y - y coordinate
 * @private
 * @returns {boolean} - Returns true, if the position is inside the linkedArea,
 * false if it is outside.
 */
var isInside = function (linkedArea, pos) {
  // The rect of each LinkedArea (or the outer container) can be scaled, therefore we must consider the scale x and scale y!
  var rect = {};
  rect.x = linkedArea.rect.x * MTLG.getStageContainer().scaleX;
  rect.w = linkedArea.rect.w * MTLG.getStageContainer().scaleX;
  rect.y = linkedArea.rect.y * MTLG.getStageContainer().scaleY;
  rect.h = linkedArea.rect.h * MTLG.getStageContainer().scaleY;
   return rect.x < pos.x && pos.x < rect.x + rect.w && rect.y < pos.y && pos.y < rect.y + rect.h;
}

/**
 * Handler function registered for pressup events of SharedObjects, that are dragged inside the LinkedArea
 * @param {object} event The triggered pressup event
 * @private
 * @alias LinkedArea.pressupHandler
 */
var pressupHandler = function (event) {
    // Get the position of the CreateJS object
    var createJsObj = event.currentTarget;
    var pos = createJsObj.localToGlobal(createJsObj.regX, createJsObj.regY);
    for (var l of linkedAreas) {
        // For each LinkedArea, check if the createJsObj is inside
        if (isInside(l, pos)) {
            var sharedId = createJsObj.sharedId;
            var sharedObj = MTLG.distributedDisplays.SharedObject.getSharedObject(createJsObj.sharedId); // Get the wrapping SharedObject
            MTLG.distributedDisplays.communication.sendSharedObject(l.receiver, sharedObj, l.independentCopyThere, l.deleteHere); // Send the SharedObject to the linked receiver
            // Remove SharedObject from the list of SharedObjects inside l if deleted here
            if (l.deleteHere) {
                for(var i of linkedAreas) {
                    i.sharedIdsInside.delete(sharedId);
                    if (i.sharedIdsInside.size === 0) {
                        i.appearance.graphics._fill.style = i.config.fill;
                        i.appearance.graphics._stroke.style = i.config.stroke;
                    }
                }
            }
            return;
        }
    }
};

/**
 * Handler function registered for the addSharedObjects action.
 * Moves the received SharedObject inside of the LinkedArea that links to the sender of the SharedObjects<p hidden>
 ** @param {{action: "addSharedObjects", added: {[sharedId: string]: SharedObject}, from: string}} action The received action</p>
    * @param {{action: "addSharedObjects", added: Object.<string, SharedObject>, from: string}} action The received action
    * @private
    * @alias LinkedArea.receivedSharedObjectHandler
    */
var receivedSharedObjectHandler = function (action) {
    var senderArea;
    // Search for the LinkedArea that is linked to the sender of the action
    for (var l of linkedAreas) {
        if (l.receiver === action.from) {
            senderArea = l;
            break;
        }
    }

    // If such a LinkedArea exists, move the received SharedObject to its center
    if (senderArea) {
        var center = { // The center of the senderArea
            x: senderArea.rect.x + (senderArea.rect.w / 2.0),
            y: senderArea.rect.y + (senderArea.rect.h / 2.0)
        };
        for (var i in action.added) { // For every added SharedObject
            if (action.toAdd[i].isRoot) { // If the corresponding sendable was the root of the transmitted sendables
                // Move the SharedObject to the center of the senderArea
                var createJsObj = action.added[i].createJsObject;
                action.added[i].justReceived = {
                    propertyPath: "x",
                    newValue: center.x
                };
                createJsObj.x = center.x;
                action.added[i].justReceived = {
                    propertyPath: "y",
                    newValue: center.y
                };
                createJsObj.y = center.y;
            }
        }
    }
};




/* Private static properties */

/**
 * Is the initialization already done.
 * Used to register a custom function for the addSharedObjects action, which has to be done only once and after the actions file has been parsed.
 * @private
 * @type {boolean}
 * @alias LinkedArea.initialized
 */
var initialized = false;

/**
 * Set of all LinkedAreas
 * @private
 * @type Set.<LinkedArea>
 * @alias LinkedArea.linkedAreas
 */
var linkedAreas = new Set();

/**
 * WeakSet of all SharedObject, where the pressupHandler is registered as a listener for the pressup event
 * @private
 * @type WeakSet.<SharedObject>
 * @alias LinkedArea.pressupRegistered
 */
var pressupRegistered = new WeakSet();


export default LinkedArea;
